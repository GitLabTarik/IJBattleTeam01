// Author: Team IJB01 Jesse Klijn
// Laatste edit: 15-06-16


//TEST DATA VOOR QUIZ SCENARIO
var $vraag = $(".vraag");
var vragen = new Array("Wat is 1+1?",
				"Wat is 'hallo' in het engels?",
				"Onder hoeveel graden Celcius bevriest water?",
				"Hoeveel euro-cent zit er in één euro?",
				"Hoeveel dagen heeft februari in een schrikkel jaar?",
				"Hoeveel maanden heeft één jaar?",
				"Hoeveel minuten zitten er in een uur?",
				"Wat is 5+3?",
				"Wat is 10-3?",
				"Hoeveel euro-cent is 2 euro + 25 cent?");
var currentVraag = 0; 
var answeredElement = 0; //element id in the the "antwoorden"
var correctAnswer = 0;
var time = 10;
var isMakingQuiz = false;
var _OFFSET = 1; //Vragen moeten met 1 beginnen, in een array is dit niet het eerste element, en daarom gebruiken wij een FINAL
var antwoorden = new Array("1","2","3","4",
					"Hello","Guten tag", "Bonjour", "Bouenos dias",
					"0","50","20","10",
					"1","10","100","1000",
					"27","28","29","30",
					"10","9","11","12",
					"600","6","60","10",
					"5","3","15","8",
					"13","10","7","103",
					"2.25","22.5","52","225");

var goedeAntwoorden = new Array("2",
						"Hello",
						"0",
						"100",
						"29",
						"12",
						"60",
						"8",
						"7",
						"225");
var ingevuldeAntwoorden = new Array(); //Lijst met ingevulde antwoorden. (door speler ingevuld)
var resultaten = new Array(); //Lijst met antwoorden of ze "goed" of "fout" zijn. 
var uitlegText = new Array("Excepteur sint occaecat cupidatat non proident",
					"Het antwoord is hello omdat het uit het latijn komt voor Hellus",
					"Het vriespunt ligt op de null","er zit honderd(100) cent in een euro",
					"Er zitten negen en-twintig(29) dagen in februari dit jaar.",
					"Er zitten twaalf maanden, vier kwartalen en 365 dagen in dit jaar",
					"er zitten zestig minuten in een uur",
					"5+3 is ook wel 2+3+3, dus daarbij 8",
					"10 - 3 is eigenlijk drie van tien afhalen, dus dan krijg je 7",
					"Dat is 250 euro cent, omdat elke euro 100 cent waard is.");
//test antwoorden invullen en openieuw proberen 


//Einde variabelen

//Voert één malig een hide functie uit
hideAllContainers();

//JQuery library ophalen uit rootfolder
$.getScript("jquery1.12.3.js", function(){});

//Dit is de event-listener voor het wachten op user input.
$(document).ready(function(){	
	$("#tableContent").on("click","tr", function(e) { 
	var select = $(this);
	var id = select.attr('id');
	//$("#"+id).css("background-color","black");

	var split = id.split("resultaat");
	var splitted = split[1];
	
	  //var uitkomst = $("#uitkomst").text();
	  
		if(isNumeric(splitted) == true){
		 checkTooltip(parseInt(splitted));
		}
	 
	});
	$("#leerlingTable").on("click","#verwijderTD", function(e) { 
	
	
	var select = $(this).children("a");
	var id = select.attr('id');
	//$("#"+id).css("background-color","black");

	var split = id.split("verwijder");
	var splitted = split[1];
	
	  //var uitkomst = $("#uitkomst").text();
	  
		if(isNumeric(splitted) == true)
		{	
			if (confirm("Wil je echt de leering verwijderen?") == true) {
				
				window.location.href = ("leerlingVerwijderen.php?id="+splitted);
			} else {
			
			}
		}
	});
	$("#docentTable").on("click","#verwijderTD", function(e) { 
	
	
	var select = $(this).children("a");
	var id = select.attr('id');
	//$("#"+id).css("background-color","black");

	var split = id.split("verwijder");
	var splitted = split[1];
	
	  //var uitkomst = $("#uitkomst").text();
	  
		if(isNumeric(splitted) == true)
		{	
			if (confirm("Wil je echt de docent verwijderen?") == true) {
				
				window.location.href = ("docentVerwijderen.php?id="+splitted);
			} else {
			
			}
		}
	});
	$("#Answer0").click(function() {  
		answeredElement = (currentVraag * 4);
		storeAnswers(answeredElement);
	});
	$("#Answer1").click(function() {  
		answeredElement = ((currentVraag * 4)+1);
		storeAnswers(answeredElement);
	});
	$("#Answer2").click(function() {  
		answeredElement = ((currentVraag * 4)+2);
		storeAnswers(answeredElement);
	});
	$("#Answer3").click(function() {  
		answeredElement = ((currentVraag * 4)+3);
		
		storeAnswers(answeredElement);
	});
	
	$(".Answer").click(function() {

	//alert("Een antwoord is geklikt! Antwoord: "+ antwoorden[answeredElement]);
	nextQuestion();
	showQuestionCase();
	
	
	});
	//Laat de quizz container zien en hide alle containers
    $("#quizz").click(function(){
        hideAllContainers();
		showPin();
    });
	//Laat alle home containers zien en hide alle containers
	$("#home").click(function(){
        hideAllContainers();
    });
	//De quiz start als op de button wordt gedrukt.
	$("#docentBeheer").click(function(){
	  hideAllContainers();
	  $("#docentBeheer-container").show();
	});
	$("#stats").click(function(){
	  hideAllContainers();
	  $("#stats-container").show();
	});
	$("#settings").click(function(){
	  hideAllContainers();
	  $("#settings-container").show();
	});
	$("#leerlingBeheer").click(function(){
	  hideAllContainers();
	  $("#leerlingBeheer-container").show();

	});
	$("#quizBeheer").click(function(){
	  hideAllContainers();
	  $("#quizBeheer-container").show();
	});
	$("#done").click(function(){
		//dit reset de quiz
		$("#quizresult-container").hide();
				resultaten = [];
		ingevuldeAntwoorden = [];
		isMakingQuiz = false;
	
	});
});
function hideAllContainers() { //Dit laat alle containers die in de methode zijn toegevoegd niet zien.
	//voeg hier een container toe en voer in de ID of Class tussen de " "
	$("#test").hide();
	$(".pin-container").hide();
	$("#quizz-question-container").hide();
	$("#quizBeheer-container").hide();
	$("#leerlingBeheer-container").hide();
	$("#stats-container").hide();
	$("#settings-container").hide();
	$("#quizresult-container").hide();
	$("#docentBeheer-container").hide();
}
function showPin(){
	$(".pin-container").show();
}
function showQuiz(){
	hideAllContainers();
	$("#quizz-question-container").show();
	isMakingQuiz = true;
	$(".sideNav").hide();
}
function showQuizResults(){
	hideAllContainers();
	$("#quizresult-container").show();
}
function checkTooltip(id){
	
	if($("#tooltip").length == 0) {
	createTooltip(id);
	}
	else{
	$("#tooltip").remove();
	createTooltip(id);
	}			
}
function createTooltip(id){
	var tooltipContainer = $("<div></div>").attr("id",("tooltip")).css({"float":"left"});
	var questionNumber = $("<label></label>").text("["+(id +1) + '/' + vragen.length + "]").css("float","left");
	var question = $("<label></label>").attr("id","questionName").text(vragen[(id)]).css("float","left");
	var answer1 = $("<button></button>").attr("id",("answer0")).css("float","left").text(antwoorden[(id*4)]);
	var answer2 = $("<button></button>").attr("id",("answer1")).css("float","left").text(antwoorden[((id*4)+1)]);
	var answer3 = $("<button></button>").attr("id",("answer2")).css("float","left").text(antwoorden[((id*4)+2)]);
	var answer4 = $("<button></button>").attr("id",("answer3")).css("float","left").text(antwoorden[((id*4)+3)]);
	$( "#tableContent" ).after(tooltipContainer);
	tooltipContainer.append(questionNumber,question,answer1,answer2,answer3,answer4);
	var a = 0;
	for (i = (id * 4); i <= ((id * 4) + 3); i++) 
	{

		if(antwoorden[i] == goedeAntwoorden[id]){
		$("#answer"+a).css({"background-color":"A1D490"});
		}
		else {
		$("#answer"+a).css("background-color","#D4A190");
		}
		if(ingevuldeAntwoorden[id] == antwoorden[i]){
		$("#answer"+a).css({"border":"solid 1px yellow"});
		
		}
		
		
		if(a < 3){a++;}
		else { a = 0;}
		
	}
	var uitlegDiv = $("<div></div>").attr("id","uitlegDiv").css({"float":"left","border":"black 1px solid","width":"90%","margin-left":"5%","background-color":"white","border-radius":"5px","height":"175px"});
	var uitlegTitel = $("<label></label>").attr("id","uitlegTitel").text("Uitleg:").css("float","left");
	var uitleg = $("<label></label>").attr("id","uitleg").text(uitlegText[id]).css("float","left");
	var breaker = $("</br>");
	tooltipContainer.append(uitlegDiv);
	uitlegDiv.append(uitlegTitel,breaker,uitleg);
	
	

}
function fillresultTable(){
	//Vult de resultaten tabel met een for loop om elke vraag in de tabel te stoppen.
	for (x = 0; x <= (vragen.length -1); x++) 
	{	
	
		var result = $("<tr></tr>").attr('id', ('resultaat'+x));
		
		//Maak een table row voor alle data en stop die in #tablecontent
		$("#tableContent").append(result);
		
		//Maak de html data aan
		var row  = $("<tr></tr>").attr("id",("row"+x)).css({"border":"black 1px solid","background-color":"#CCD5D9"}).css("min-width","200px");
		var question = $("<td></td>").attr("id","vraagNummer").text(vragen[x]);
		var rightAnswer = $("<td></td>").attr("id","rightAnswer");
		var filledAnswer = $("<td></td>").attr("id","filledAnswer");
		
		//Vul de row aan de tabel toe
		$(("#resultaat"+x)).append(row);
		$(("#row"+x)).append(question,rightAnswer,filledAnswer);
		
		//Is het ingevulde antwoord goed?
		if(goedeAntwoorden[x] === ingevuldeAntwoorden[x]){
		
		filledAnswer.css("background-color","#A1D490");
		filledAnswer.append('<img id="theImg" width="25px" src="Images/good.png" />')
		}
		
		else if(goedeAntwoorden[x] !== ingevuldeAntwoorden[x]){
		filledAnswer.css("background-color","#D4A190");
		filledAnswer.append('<img id="theImg" width="25px" src="Images/bad.png" />')
		
		}
		
	}
	
}
function showQuestionCase(){
	//Methode die één vraag scenario vult
	isMakingQuiz = true;
	
	//Vraag data invullen
	var questionNumber = "[" +(currentVraag+_OFFSET) + "/" + vragen.length + "]";
	$(".questionNumber").text(questionNumber);
	$(".questionName").text(vragen[currentVraag]);
	
	//Tweede increment nodig voor elk antwoord element te vullen
	var a = 0;
	//For loop is nodig om elke vraag met vier antwoorden te vullen in de antwoord buttons
	for (i = (currentVraag * 4); i <= ((currentVraag * 4) + 3); i++) 
	{
		$("#Answer"+[a]).prop('value', antwoorden[i]);
		if(a < 3){a++;}
		else { a = 0;}
	}
}
function storeAnswers(enteredAnswer){
	//Deze methode vult de antwoorden in de arrays zodat dit gebruikt kan worden in de database -
	//en toegevoegd kan worden aan het resultaten overzicht na de quiz.
	if(antwoorden[enteredAnswer] ==  goedeAntwoorden[currentVraag]){
		resultaten.push("goed");
		ingevuldeAntwoorden.push(antwoorden[enteredAnswer]);
	}
	else {
		resultaten.push("fout");
		ingevuldeAntwoorden.push(antwoorden[enteredAnswer]);
	}
}
function isNumeric( obj ) {
	//Methode die kijkt of een variabele een numeric value is. 
	//Hij kijkt bijvoorbeeld of "15" dat als string datatype is opgeslagen een integer kan zijn.
    return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
}
function nextQuestion(){
	//Dit reset de vraag timer en verhoogt de vraag bij 1.
	//Of als alle vragen zijn gemaakt reset hij de data en geeft de resultaten tabel weer.
	if((vragen.length-_OFFSET) != currentVraag)
	{
		currentVraag++;
		time = 10; //reset timer
		
	}
	else
	{
		isMakingQuiz = false;
		fillresultTable(); //Resultaten tabel vullen

		
		currentVraag = 0;
		var answeredElement = 0;
		var correctAnswer = 0;
		
		//geef gebruiker feedback
		alert("Alle vragen zijn gemaakt!");
		
		$("#quizz-question-container").hide();
		$(".sideNav").show();
		$("#quizresult-container").show();
		

	}
}
function countDown(secs,elem){
	//Countdown functie die is geschreven door Peter, en vervolgens ge-edit door Jesse Klijn
	if(isMakingQuiz == true){
		//Vult de timer in de vraag scenario
		var element = document.getElementById(elem);
		element.innerHTML = "["+time+"]";
		
		if (time < 1){
			//Gaat naar de volgende vraag
			secs = time;
			nextQuestion();
			ingevuldeAntwoorden.push("");
			resultaten.push("fout");
			showQuestionCase();
			
		}
		time--;
		
	}
	var timer = setTimeout('countDown('+time+',"'+elem+'")', 1000);
}
