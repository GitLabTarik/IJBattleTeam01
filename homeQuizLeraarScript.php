<?php
session_start();
$maxLength = 20; // maximum length of category or quiz

//select function by button and connect php homeQuizLeraarScript with html homeQuizLeraar

//var_dump($_POST);

if (isset($_POST['back_to_homeQuizLeraar'])){
	$tekst = "quizBeheer-container";
	$_SESSION['container'] = $tekst;
	returnToHTML("Location: controlpanel.php");
} else if (isset($_POST['button_newCategory'])){
	createHTMLnewCategory();
} else if (isset($_POST['button_newQuiz'])){
	createHTMLnewQuiz();
} else if (isset($_POST['button_changeCategory'])){
	createHTMLchangeCategory();
} else if (isset($_POST['button_changeQuiz'])){
	createHTMLchangeQuiz();
} else if (isset($_POST['button_deleteCategory'])){
	createHTMLdeleteCategory();
} else if (isset($_POST['button_deleteQuiz'])){
	createHTMLdeleteQuiz();
} else if(isset($_POST['submit_newCategory'])){
	$newCategory = $_POST['submit_newCategory'];
	createCategory($newCategory);
} else if(isset($_POST['submit_change_categorie'])){
	$newCategoryName = $_POST['categorie_name'];
	$selectCategory = $_POST['categorie'];
	changeCategory($selectCategory, $newCategoryName);
} else if(isset($_POST['submit_delete_categorie'])){
	$deleteCategory = $_POST['categorie'];
	deleteCategory($deleteCategory);
} else if(isset($_POST['submit_create_newQuiz'])){
	$newNameQuiz = $_POST['newNameQuiz'];
	$newStatus = $_POST['status'];
	$selectCategory = $_POST['categorie'];
	createQuiz($newNameQuiz,  $newStatus, $selectCategory);
} else if(isset($_POST['submit_change_quiz'])){
	$nameQuiz = $_POST['nameQuiz'];
	$newNameQuiz = $_POST['newNameQuiz'];
	$selectCategory = $_POST['categorie'];
	$newStatus = $_POST['status'];
	changeQuiz($nameQuiz, $newNameQuiz, $selectCategory, $newStatus);
} else if(isset($_POST['submit_delete_quiz'])){
	$deleteQuiz = $_POST['nameQuiz'];
	deleteQuiz($deleteQuiz);
}




/* 
====================================================
  Helper functions
====================================================
*/

//back to hmtl homeQuizLeraar page
function returnToHTML($url){
	//sleep(3);
	header($url);
	exit();

}


//html generate functions
function head($title)
{
   $title = htmlentities($title);
   return <<<END
<html>
<head><title>$title</title></head>
<body>
<!-- anything else that goes before dynamic content -->
END;
}

function foot()
{
   return <<<END
<!-- anything that comes after the dynamic content -->
</body>
</html>
END;
} 


//function to connect to mySQL DB
function connectDB(){
	$username = "root";
	$password = "";
	$hostname = "localhost:3306"; 
	$dbname = "ijbattle";

	//connection to the database
	$dbhandle = mysqli_connect($hostname, $username, $password, $dbname) 
	  or die("Unable to connect to MySQL");
	//echo "DEBUG: Connected to MySQL<br>";
	return $dbhandle;
}



/* 
====================================================
  Create HTML functions
====================================================
*/

//function to create html for new category
function createHTMLnewCategory(){
	
	echo head("Create Category");
	echo '<form method="post" action="homeQuizLeraarScript.php">
					<div class="text1">
						<font size="10"><b>Enter name of new category: </b></font>
					</div>
					<input type="text" name="submit_newCategory" placeholder="New Category"/>
					<input type="submit" value="Enter"/>
					<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>
				</form>';
	echo foot();
}


//function to create html for new quiz
 function createHTMLnewQuiz(){
	 
	list ($nameQuiz, $idCategorie, $status) = selectQuiz();
	$categories = selectCategory();
	
	echo head("Create Quiz");
	echo '<form method="post" action="homeQuizLeraarScript.php">
					<div class="text1">
						<font size="10"><b>Enter name of new quiz, new category id and new status: </b></font>
					</div>
					<input type="text" name="newNameQuiz" placeholder="New Quiz name"/>
					</br>
					<select name="categorie">
						<option value="">Select categorie...</option>';
					
	foreach ($categories  as $x => $row){
	// vul drop down		
		echo "<option value=\"$row\">$row</option> </br>";
	};
					
					
	echo '			</select>
					<br>
					<select name="status">
						<option value="">Select status...</option>';
					
	
	// vul drop down
			echo "<option value=0>Uit</option> </br>";
			echo "<option value=1>Aan</option> </br>";
		
		//echo "<option value=\"$row\">$row</option> </br>";
	
	echo '			</select>
					<br>
					<input type="submit" name="submit_create_newQuiz" value="Enter"/>
					<!--<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>-->
				</form>';
	echo foot();
}


//function to create html for change category
 function createHTMLchangeCategory(){
	
	//echo "DEBUG: function createHTMLchangeCategory</br>";
	
	$categories = selectCategory();
	
	//echo "DEBUG: after selectCategory</br>";
	
	
	echo head("change category");
	echo '<form method="post" action="homeQuizLeraarScript.php">
					<div class="text1">
						<font size="7"><b>Selecteer een categorie en voer een nieuwe naam in: </b></font>
					</div>
					
					<select name="categorie">
					  <option value="">Select categorie...</option>';
					  
	foreach ($categories as $x => $row){
	// vul drop down		
		echo "<option value=\"$row\">$row</option> </br>";
	};
					  
					  
					  
	echo '			</select>
					<b>verander categorie in: </b>
					<input type"text" name="categorie_name" placeholder="new category name"/>
					</br>
					<input type="submit" name="submit_change_categorie" value="Enter"/>
					<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>
				</form>';
	echo foot();
}


//function to create html for change category
 function createHTMLchangeQuiz(){
	
	//echo "DEBUG: function createHTMLchangeQuiz</br>";
	
	list ($nameQuiz, $idCategorie, $status) = selectQuiz();
	$categories = selectCategory();
	
	
	//echo "DEBUG: after selectQuiz</br>";
	
	echo head("change Quiz");
	echo '<form method="post" action="homeQuizLeraarScript.php">
					<div class="text1">
						<font size="10"><b>Enter name of new quiz, new category id and new status: </b></font>
					</div>
					
					<select name="nameQuiz">
						<option value="">Selecteer een quiz...</option>';
						
					
	foreach ($nameQuiz as $x => $row){
	// vul drop down		
		echo "<option value=\"$row\">$row</option> </br>";
	};
					
	echo '			</select>
	
	<input type="text" name="newNameQuiz" placeholder="New Quiz name"/>
	<br>
					<select name="categorie">
						<option value="">Selecteer nieuwe categorie...</option>';
						
						
					
	foreach ($categories as $x => $row){
	// vul drop down		
		echo "<option value=\"$row\">$row</option> </br>";
	};
					
	echo '			</select>
					<br>
					<select name="status">
						<option value="">Selecteer nieuwe status...</option>
					
	// vul drop down
			 <option value=0">Uit</option> </br>;
			"<option value=1">Aan</option> </br>;
			
							
				</select>
				<br>
					<input type="submit" name="submit_change_quiz" value="Enter"/>
					<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>
				</form>';
				
	echo foot();
}


//function to create html for change category
 function createHTMLdeleteCategory(){
	
	//echo "DEBUG: function createHTMLdeleteCategory</br>";
	
	$categories = selectCategory();
	
	//echo "DEBUG: after selectCategory</br>";
	
	
	echo head("delete category");
	echo '<form method="post" action="homeQuizLeraarScript.php">
					<div class="text1">
						<font size="10"><b>Enter name of new quiz, category id and status: </b></font>
					</div>
					
					<select name="categorie">
					  <option value="">Select categorie...</option>';
					  
	foreach ($categories as $x => $row){
	// vul drop down		
		echo "<option value=\"$row\">$row</option> </br>";
	};
					  
					  
					  $selected = $_POST['$categories'];
	echo '			</select>
					</br>
					<input type="submit" name="submit_delete_categorie" value="Enter"/>
					<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>
				</form>';
	echo foot();
}


//function to create html for change category
 function createHTMLdeleteQuiz(){
	
	list ($quiz, $idCategorie, $status) = selectQuiz();

	echo head("delete Quiz");
	echo '<form method="post" action="homeQuizLeraarScript.php">
						<div class="text1">
						<font size="10"><b>Enter name of new quiz, new category id and new status: </b></font>
					</div>
					
					<select name="nameQuiz">
						<option value="">Select quiz...</option>';
					
	foreach ($quiz as $x => $row){
	// vul drop down		
		echo "<option value=\"$row\">$row</option> </br>";
	};
					
					
	echo '			</select>
					<br>
					<input type="submit" name="submit_delete_quiz" value="Enter"/>
					<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>
				</form>';
	echo foot();
}



/* 
====================================================
  Main Category functions
====================================================
*/

//function to select category
function selectCategory(){
	//echo "DEBUG: function selectCategory</br>";
	
	
	$handle = connectDB();
	//select all categories from DB
	$query = sprintf("SELECT * FROM categorie");
	$result = mysqli_query($handle, $query);
	//echo "DEBUG: after query</br>";
	
	$categories = array();
	
	while($row = mysqli_fetch_assoc($result)){
		//var_dump($row['categorie']);
		array_push($categories, $row['categorie']);
	}
	
	return $categories;
	
}

//function to select quiz
function selectQuiz(){
	//echo "DEBUG: function selectQuiz</br>";
	
	
	$handle = connectDB();
	//select all categories from DB
	$query = sprintf("SELECT * FROM quiz");
	$result = mysqli_query($handle, $query);
	//echo "DEBUG: after query</br>";
	
	$nameQuiz = array();
	$idCategorie = array();
	$status = array();
	
	while($row = mysqli_fetch_assoc($result)){
		//$var1 = $row['idQuiz', 'nameQuiz', 'idCategorie', 'status'];
		array_push($nameQuiz, $row['nameQuiz']);
		array_push($idCategorie, $row['idCategorie']);
		array_push($status, $row['status']);
	}
	
	return array ($nameQuiz, $idCategorie, $status);
	
}
 
//function to create new category
function createCategory($category){
	//echo "dit is de nieuwe category: $category</br>";
	
	
 	//controleer geldigheid invoer string moet voldoen aan regels db
	//check user input([a-zA-Z0-9_] and length < 20)

	$handle = connectDB();
	
	//query select nieuwe categorie from categorie
	//if category already exists ask for new input

	//als categorie niet bestaat dan query insert into categorie
	//query to insert new category into DB
	$query = sprintf("INSERT INTO categorie (categorie) VALUES ('$category')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	
	if ($result){
		echo "DEBUG: Opslaan new categorie $category gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	*/
	// terug naar homeQuizLeraar.php
	$tekst = "quizBeheer-container";
	$_SESSION['container'] = $tekst;
	returnToHTML("Location: controlpanel.php");

}



//function to change category
function changeCategory($select, $category){
	echo "DEBUG: function changeCategory: categorie heeft nu $category</br>";

	
	$handle = connectDB();
	
	//query update into DB
	$query = sprintf("UPDATE categorie categorie SET categorie = '$category' WHERE categorie = '$select' ");
	$result = mysqli_query($handle, $query);
			  $tekst = "quizBeheer-container";
		$_SESSION['container'] = $tekst;
	 returnToHTML("Location: controlpanel.php");
}



//function to delete category
function deleteCategory($select){
	echo "DEBUG: function deleteCategory: categorie die verwijderd wordt: category</br>"; //categorie = $category
	
	$handle = connectDB();
	
	//query delete from DB
	$query = sprintf("DELETE FROM categorie WHERE categorie = '$select'");
	$result = mysqli_query($handle, $query);
	echo $query;
	$tekst = "quizBeheer-container";
		$_SESSION['container'] = $tekst;
	 returnToHTML("Location: controlpanel.php");
}



/* 
====================================================
  Main Quiz functions
====================================================
*/

//function to create new quiz
function createQuiz($nameQuiz, $status, $select){
	//echo "dit is de nieuwe quiz: $nameQuiz</br>";
	
	
 	//controleer geldigheid invoer string moet voldoen aan regels db
	//check user input([a-zA-Z0-9_] and length < 20)

	 $handle = connectDB();
		
		// selecteer de categorie door inner join, je kiest bijv Rekenen uit de dropdown en dan kijkt hij naar de idCategorie die erbij hoort en slaat die op in een variable.
		$query = sprintf("SELECT c.idCategorie FROM categorie c, quiz q WHERE c.idCategorie = q.idCategorie AND c.categorie = '$select'");
		$result = mysqli_query($handle, $query);
		
		$array = array();
		$row=$result->fetch_assoc();
		$id = intval($row['idCategorie']);
		//
		
		// insert new quiz into database
		$handle = connectDB();
		  $query = sprintf("INSERT INTO quiz (nameQuiz, idCategorie, status) VALUES ('$nameQuiz', $id, $status)");
		  $result = mysqli_query($handle, $query);
		  $tekst = "quizBeheer-container";
		$_SESSION['container'] = $tekst;
	 returnToHTML("Location: controlpanel.php");

	
}


//function to change quiz
function changeQuiz($select, $quizname, $categorie, $status) {
	//echo "DEBUG: function changeQuiz: $oldquiz veranderd in: $newquiz</br>"; //zie changeCategory
	
	
	$handle = connectDB();
	
	$query = sprintf("SELECT c.idCategorie FROM categorie c, quiz q WHERE c.idCategorie = q.idCategorie AND c.categorie = '$categorie'");
		$result = mysqli_query($handle, $query);
		
		$array = array();
		$row=$result->fetch_assoc();
		$id = intval($row['idCategorie']);
		
		$handle = connectDB();
	
	$query = sprintf("UPDATE quiz SET nameQuiz='$quizname' idCategorie=$id status =$status WHERE nameQuiz = '$select' ");
	$result = mysqli_query($handle, $query);
	$tekst = "quizBeheer-container";
	$_SESSION['container'] = $tekst;
	returnToHTML("Location: controlpanel.php");
}


//function to delete quiz
function deleteQuiz($select){
	echo "DEBUG: function deleteQuiz: quiz die verwijderd wordt: $select</br>"; //zie deleteCategory
	
	$handle = connectDB();
	
	//query delete from DB
	 $query = sprintf("DELETE FROM quiz WHERE nameQuiz = '$select'");
	$result = mysqli_query($handle, $query);
	echo $query;
	$tekst = "quizBeheer-container";
	$_SESSION['container'] = $tekst;
	returnToHTML("Location: controlpanel.php");
}


?>