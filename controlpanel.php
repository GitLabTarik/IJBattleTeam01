<!-- Author: Team IJB01 Jesse Klijn
Laatste edit: 15-06-16 -->


<?php //Login authentication check

session_start();

if ($_SESSION['login-check'] != "1"){
	
	header('Location: index.php');
}
?>
<?php //Test gegevens
if(isset($_SESSION['container']) && !empty($_SESSION['container'])){
$container = $_SESSION['container'];

}
else{

$container = '';
}

$username = $_SESSION['dbnaam'];
$type = $_SESSION['dbtype'];


?>
<?php //Timeout login authentication check
// de limit variable is de hoeveelheid secondes van inactiviteit voordat je sessie gestopt wordt.
$limit = 900;
//Als de sessie variable $_SESSION['authenticated'] null is, wordt je teruggestuurd naar index.php
if (!isset($_SESSION['authenticated'])){
	header ('Location: index.php');
	exit;
}
// deze elseif zal je terugsturen naar de index.php pagina nadat $limit aantal secondes geen activiteit is geweest, expired is gezet op true
elseif ($_SESSION['authenticated'] + $limit < time()){
	$_SESSION = [];
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time() - 86400, '/');
}
session_destroy();
header('Location: index.php?expired=true');
exit;
	
}
//als er wel activiteit is zal $_SESSION['authenticated'] gereset worden.
else {
	$_SESSION['authenticated'] = time();
	}
?>
<html>
	<head> <!--HEADER Alle referenties en title -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!--Alle css referenties -->
		<?php 
		if($_SESSION['dbtype'] == "Leerling"){
		echo '<link rel="stylesheet" type="text/css" href="controlPanelStyleSheet.css" />';
		}
		else {
			echo"<link rel='stylesheet' type='text/css' href='controlPanelStyleSheetDocent.css'/>";
		}
		?>
		
		<link rel="stylesheet" type="text/css" href="mainStyleSheet.css" />
		<link rel="stylesheet" type="text/css" href="invoercodeStyleSheet.css" />
		<link rel="stylesheet" type="text/css" href="vragenConceptCSS.css" />
		<link rel="stylesheet" type="text/css" href="homeQuizLeraarStyleSheet.css" />
		<!--Einde css referenties -->
		<!--Alle javascript referenties -->
		<script src="jquery1.12.3.js"></script>
		<script src="controlPanel.js"></script>
		<!--Einde javascript referenties -->
		
		<title> IJBattle Control Panel </title>
		
	</head>
	
	<body class="controlPanel"> <!-- De control panel body class-->
		<font face="Helvetica"> <!--Font voor de gehele pagina -->
			<?php 
		if($_SESSION['dbtype'] == "Leerling"){
			echo '<div class="sideNav">';
		}
		else if($_SESSION['dbtype'] == "Docent"){
			echo '<div class="sideNav" style = background-color:#920202>';
		}
		else{
			echo '<div class="sideNav" style = background-color:black>';
		}
		?>
		<!--Navigatie-container aan de linkerzijde van het scherm -->
			
				<ul> <!-- Unordered list voor alle navigatie items -->
					<a href="#">
						<div class="img"><img id="logo" src="Images/logo.png" alt="IJBurg College">
						</div>
					</a>
					<?php 
					
					echo "<li id='home'><a href='#'><span> Home</span></a></li>
					<li id='quizz'><a href='#'><span>Quiz spelen</span></a></li>";
					
					//TODO: SESSION vraag Jesse voor uitleg
					if($type == "Leerling"){
					echo"
					";
					}
					//TODO: SESSION
					if($type == "Docent" || $type =="Admin"){
					echo "
					<li id='quizBeheer'><a href='#'><span>Quiz beheer</span></a></li>
					<li id='leerlingBeheer'><a href='#'><span>Leerling beheer</span></a></li>
					";
					}
					if($type == "Admin"){
					echo "<li id='docentBeheer'><a href='#'><span>Docent beheer</span></a></li>";
					}
					
					echo "
					<li><a href='uitloggen.php'><span>Log uit</span></a></li>";
					?>
				</ul>
			</div>
			<div class="topNav"> <!--Navigatie-container aan de top van het scherm -->
				<div class="topContent"> <!-- Alle inhoud van de topnavigatie -->
				<font face="Helvetica" size="4">Welkom terug, <?php echo $username . "/ ". $type; ?> </font>
			</div>
			</div>
			<div id="content"> <!--Content -->
				<div class="panel"> <!-- Hierin staan de resultaten van de quiz-->
					<div id="quizresult-container">
							
							<table id="tableContent">
							<!-- Hierin wordt via javascript de tabel gevuld met data -->
							</table>
							<input id="done" type="button" value="quiz sluiten"> </input>
					
					</div>
				</div>
				<div class="panel"> <!-- Hierin staat de html voor de pin-container -->
				
					<div class="pin-container"> <!-- Hier vult de gebruiker zijn pincode in  -->
						<form method="post" action="">
									<div class="title">
										<font size="10"><b>Voer de quiz pin in!</b></font>
									</div>
							<input type="text" id="textholder" name="pin" placeholder="Voer de quiz pin in bijv. 4321"/>
							</br>
							<input type="submit" id="startQuizz" name="button1" value="Start Quiz"/>
							<?php 
							if($message = null){ $message = "Voer je code in!"; }
							echo $message;
							
							?>
							<input type="hidden" name="request" value="pin">
						</form>
					</div>
				</div>
				<div class="panel"> <!--Quiz scenario-container -->
					<div id="quizz-question-container">
						<form method="post" action="">
							 <!--php code om de kleuren willekeurig te veranderen-->
							<?php 
									
								//Variabele die later een echo krijgen om de kleur van de achtergrond aan te passen.
								$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
								$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
								$color1 = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
								$color2 = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
								$color3 = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
								$color4 = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
							?>
							<!-- Div voor de doos om de vraag heen -->
							
							<!-- Hierin staat de current vraag, en de countdown timer voor het aftellen van de resterende tijd -->
							<label class="questionNumber"></label> <label id="status">	<script type="text/javascript">countDown(10, "status");</script></label> 
							<div class= "Question">

							<h1> <label class="questionName"></label></h1>
							<!-- de 10 seconden is het aantal seconden dat je hebt om te antwoorden -->
						
							</div>
							<!-- De knoppen op de pagina -->
							<!-- Button voor hidden input naar de post onderin deze file -->
							<input type="hidden" name="request" value="answer">
							
							<!-- De antwoord knoppen voor elke vraag-->
							<div class= "" style="background: echo $color1; ;"> <input type="button" class="Answer" id="Answer0" name="Answer1" value="a"/></div>
							<div class= "" style="background: echo $color1; ;"> <input type="button" class="Answer" id="Answer1" name="Answer1" value="b"/></div>
							<div class= "" style="background: echo $color1; ;"> <input type="button" class="Answer" id="Answer2" name="Answer1" value="c"/></div>
							<div class= "" style="background: echo $color1; ;"> <input type="button" class="Answer" id="Answer3" name="Answer1" value="d"/></div>
							
							<!-- Audio die afspeelt tijdens de quiz -->
							<div class= "Audio">
								<audio autoplay>
									<source src="Flite.wav">
								</audio>
							</div>
							
							<!-- De methode die als eerste wordt gebruikt om deze form te vullen. (zie controlPanel.js  304~) --> 
							<script> showQuestionCase(); </script>
						</form>
					</div>				
				</div>			
				<div class="panel"> <!--Statistieken-container -->
					<div id="stats-container">
					Stats
					</div>
					
				</div>
				<div class="panel"> <!--Settings-container -->
					<div id="settings-container">
					Settings test
					</div>
					
				</div>
				<div class="panel"> <!--Docent beheer-container -->
					<div id="docentBeheer-container">
					<?php
						include('connect-db.php'); // Connect to DB

						$sql = "SELECT * FROM docent
								INNER JOIN
								stamgroep
								ON stamgroep.idStamgroep = docent.idStamgroep"; // Query to get all data from table
						$result = mysqli_query($conn, $sql); // Define variable result with the results from the query
						
						echo "<table id='docentTable' border='1' cellpadding='10'>"; // Create the table
						echo "<tr> <th>Voornaam</th> <th>Achternaam</th> <th>Stamgroep</th>
						<th>Deelschool</th> <th>E-mail</th><th>edit</th><th>del</th></tr>"; // Table headers

						if (mysqli_num_rows($result) > 0) { // Whileloop through results of database query, displaying them in the table
						
						while($row = mysqli_fetch_assoc( $result )) { // Whileloop through results of database query, displaying them in the table
							
							echo "<tr>"; // Print the data in the table rows
							echo '<td>' . $row['voornaam'] . '</td>';
							echo '<td>' . $row['achternaam'] . '</td>';
							echo '<td>' . $row['naamStamgroep'] . '</td>';
							echo '<td>' . $row['deelschool'] . '</td>';
							echo '<td>' . $row['email'] . '</td>';
							echo '<td><a href="docentBewerken.php?id=' . $row['idDocent'] . '"><img id="theImg" width="25px" src="Images/bewerk.png" /></a></td>';
							echo '<td id="verwijderTD"><a id="verwijder' . $row['idDocent'] . '" href="#"><img id="theImg" width="25px" src="Images/bad.png" /></a></td>';
							echo "</tr>"; 
						}
						
						} else { // If there are no results echo no results
						
							echo "Er zijn geen docenten gevonden";
						
						} 
						echo '<tr><td id="leerlingToevoegen"><p><a href="docentNieuw.php">Nieuwe docent toevoegen</a></p> </td></tr>';
						echo "</table>"; // Close table
						
					?>
					</div>
					
				</div>
				<div class="panel"> <!--Quiz beheer-container -->
					<div id="quizBeheer-container">
						<div class="border">
							<form action="homeQuizLeraarScript.php" method="post" id="form1">
							</form>
							<div>			
								<div class="borderText1">
									<font class="text" color="#FFFFFF"><center>Beheer categorieen</center></font>
									<ul>
										<li><button class="button1" name="button_newCategory" form="form1">Nieuwe catergorie</button></li>
										<li><button class="button2" name="button_changeCategory" form="form1">Wijzig categorie</button></li>
										<li><button class="button3" name="button_deleteCategory" form="form1">Verwijder categorie</button></li>
									</ul>
								</div>
								
								<div class="borderText2">
									<font class="text" color="#FFFFFF"><center>Beheer quizes</center></font>
									<ul>
										
										<li><button class="button4" name="button_newQuiz" form="form1">Nieuwe quiz</button></li>
										<li><button class="button5" name="button_changeQuiz" form="form1">Wijzig quiz</button></li>
										<li><button class="button6" name="button_deleteQuiz" form="form1">Verwijder quiz</button></li>
										<li><button class="button4"> <a href="vraagWeergeven.php">Nieuwe vraag</a></button></li>
									</ul>
								</div>

							</div>
							
							<!--buttons-->

							
							
							
							
						
						
							
							<div class="borderBottom">
								<!--	<form action="homeQuizLeraarScript.php">
										<button class="button7" name="button7"><font size="5">Selecteer categorie</font></button>
									<!--</form>-->
									
									<!--<form action="homeQuizLeraarScript.php">
										<button class="button8" name="button8"><font size="5">Start</font></button>
									<!--</form>-->
									
									<!--<form action="homeQuizLeraarScript.php">
										<button class="button9" name="button9"><font size="5">Selecteer quiz</font></button>
									</form>
									-->
							</div>
						
						</div>
					</div>
				</div>
				<div class="panel"> <!--Leerling beheer-container -->
					<div id="leerlingBeheer-container">
					<?php
						include('connect-db.php'); // Connect to DB

							$sql = "SELECT * FROM leerling
									INNER JOIN
									stamgroep
									ON stamgroep.idStamgroep = leerling.idStamgroep"; // Query to get all data from table
							$result = mysqli_query($conn, $sql); // Define variable result with the results from the query
							
							echo "<table id='leerlingTable' border='1' cellpadding='10'>"; // Create the table
							echo "<tr> <th>Voornaam</th> <th>Achternaam</th> <th>Stamgroep</th>
							<th>Deelschool</th> <th>E-mail</th><th>edit</th><th>del</th></tr>"; // Table headers

							if (mysqli_num_rows($result) > 0) { // If statement to only loop if there are results
							
							while($row = mysqli_fetch_assoc( $result )) { // Whileloop through results of database query, displaying them in the table
								
								echo "<tr>"; // Print the data in the table rows
								echo '<td>' . $row['voornaam'] . '</td>';
								echo '<td>' . $row['achternaam'] . '</td>';
								echo '<td>' . $row['naamStamgroep'] . '</td>';
								echo '<td>' . $row['deelschool'] . '</td>';
								echo '<td>' . $row['email'] . '</td>';
								echo '<td><a href="leerlingBewerken.php?id=' . $row['idLeerling'] . '"><img id="theImg" width="25px" src="Images/bewerk.png" /></a></td>';
								echo '<td id="verwijderTD"><a id="verwijder' . $row['idLeerling'] . '" href="#"><img id="theImg" width="25px" src="Images/bad.png" /></a></td>';
								echo "</tr>"; 
								
							}
							
							} else { // If there are no results echo no results

								echo "Er zijn geen leerlingen gevonden"; 

							} 
							echo '<tr><td id="leerlingToevoegen"><p><a href="leerlingNieuw.php">Nieuwe leerling toevoegen</a></p> </td></tr>';
							echo "</table>"; // Close table
							
					?>

					

					</div>
					
				</div>
			</div> <!--Einde content-->
			<div id="footer"> <!--Footer-container -->
			
						<!-- Dit is de facebook integratie waarbij je kunt Ijburg kunt liken. 
						De code is gekopierd van www.facebook.com -->
						<div id="footerFacebook"><iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FIJburg-College-deelschool-T2-981545421890715%2F%3Ffref%3Dts&width=450&layout=standard&action=recommend&show_faces=true&share=true&height=80&appId=813995018633406" 
							width="450" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
						<div id="footerContent">
							<i><font size="2"> Hogeschool van Amsterdam - Team IJB001 - 2015/2016</font></i>
						</div>
						
						
			</div>
		</font>
	</body>
</html>
<?php //Php sessie checks voor het openen en sluiten van containers
	echo '<script> hideAllContainers(); </script>';	
	if($container == "leerlingBeheer-container"){
	echo '<script>  $("#leerlingBeheer-container").show(); </script>';
	}
	if($container == "docentBeheer-container"){
	echo '<script>  $("#docentBeheer-container").show(); </script>';
	}
	if($container == "quizBeheer-container"){
	echo '<script>  $("#quizBeheer-container").show(); </script>';
	}
	unset($_SESSION['container']);
?>
<?php //Een self post method die kijkt of de quizpin is ingevuld
	if($_SERVER['REQUEST_METHOD'] == 'POST') 
	{ 
		$request = $_POST['request'];
		if($request === 'pin')
		{
			$Quizpin = 1111; 
			// Controle of benodigde velden wel ingevuld zijn 
			if(isset($_POST['pin'])) 
			{ 
			
				$IngevuldQuizPin = trim($_POST['pin']); 
				
				// Gebruikersnaam en wachtwoord controleren 
				if((int)$IngevuldQuizPin === $Quizpin){
					echo '<script> showQuiz(); </script>';
					die();
				}
				else if((int)$IngevuldQuizPin != (int)$Quizpin && $IngevuldQuizPin != 0){
					echo "<script> showPin(); </script>";
					echo "<script> alert('De code is onjuist! Probeer het opnieuw!');</script>";
					
					die();
				}
			}
		}
	}
?>
