<?php
session_start();
if(isset($_POST['button_newQuiz'])){
	createHTMLnieuw();
} else if(isset($_POST['button_changeQuiz'])){
	createHTMLchange();
} else if(isset($_POST['button_deleteQuiz'])){
	createHTMLdelete();
} else if(isset($_POST['annuleer'])){
	returnToHTML("Location: controlpanel.php");
} else if(isset($_POST['enterQuiz'])){
	$idQuiz = $_POST['idQuizes'];
	$newVraag = $_POST['vraag'];
	createVraag($idQuiz, $newVraag);
}

 /* 
====================================================
  help functions
====================================================
*/

//back to hmtl homeQuizLeraar page
function returnToHTML($url){
	//sleep(3);
	header($url);
	exit();

}


//html generate functions
function head($title)
{
   $title = htmlentities($title);
   return <<<END
<html>
<head><title>$title</title></head>
<body>
<!-- anything else that goes before dynamic content -->
END;
}

function foot()
{
   return <<<END
<!-- anything that comes after the dynamic content -->
</body>
</html>
END;
} 

//function to connect to mySQL DB
function connectDB(){
	$username = "root";
	$password = "admin";
	$hostname = "localhost:3306"; 
	$dbname = "ijbattle";

	//connection to the database
	$dbhandle = mysqli_connect($hostname, $username, $password, $dbname) 
	  or die("Unable to connect to MySQL");
	//echo "DEBUG: Connected to MySQL<br>";
	return $dbhandle;
}
 
 /* 
====================================================
  Create HTML functions
====================================================
*/
function createHTMLnieuw(){
	
	$categories = selectCategory();
	$idQuizes = selectIdQuiz();
	
echo head("Create Category");
echo '		
			<font class="text1" size="5">Selecteer categorie: </font>
			
			
				
				
				<select name="categorie">
					  <option value="">Select categorie...</option>';
				
				foreach ($categories as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$categories'];
				echo '</select>
			
				<br>
				<font class="text2" size="5">Quiznaam: </font>
				<input type="text" class="tekstvak1" name="QuizNaam" placeholder="Quiz naam"/>
			
			</br>
			</br>
			
			<font class="text1" size="5">Selecteer idQuizes: </font>
			
			<select name="idQuizes">
					  <option value="">Select idQuiz...</option>';
				
				foreach ($idQuizes as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$idQuizes'];
				echo '</select>
			
			
			<br>
			
				<font class="text3" size="5">Vraag 1: </font>
				<input type="text" class="tekstvak2" name="vraag" placeholder="Vraag"/>
			<br
				<!-- timer -->
				<font class="text13" size="5">Timer: </font>
				<input type="text" class="tekstvak8" name="timer" placeholder="Timer"/>
				
				
			</br>
			</br>
				<input type=checkbox name="Antwoord1">
				<font class="text4" size="5">Antwoord 1: </font>
				<input type="text" class="tekstvak3" name="Antwoord1" placeholder="Antwoord"/>
				<input type=checkbox name="CorrectAntwoord1">
				<font class="text5" size="5">Correct antwoord</font>
	
			</br>
				<input type=checkbox name="Antwoord2">
				<font class="text6" size="5">Antwoord 2: </font>
				<input type="text" class="tekstvak4" name="Antwoord2" placeholder="Antwoord"/>
				<input type=checkbox name="CorrectAntwoord2">
				<font class="text7" size="5">Correct antwoord</font>
			</br>
				<input type=checkbox name="Antwoord3">
				<font class="text8" size="5">Antwoord 3: </font>
				<input type="text" class="tekstvak5" name="Antwoord3" placeholder="Antwoord"/>
				<input type=checkbox name="CorrectAntwoord3">
				<font class="text9" size="5">Correct antwoord</font>
			</br>
				<input type=checkbox name="Antwoord4">
				<font class="text10" size="5">Antwoord 4: </font>
				<input type="text" class="tekstvak6" name="Antwoord4" placeholder="Antwoord"/>
				<input type=checkbox name="CorrectAntwoord4">
				<font class="text11" size="5">Correct antwoord</font>
			<br>
			<br>
			<!-- uitleg correct antwoord -->
				<font class="text12" size="5">Uitleg goed antwoord: </font>
				<textarea type="" class="tekstvak7" name="uitlegGoedAntwoord" placeholder="Uitleg goed antwoord"></textarea>
			</br>
			</br>
				<input type="submit" class="button1" name="addVraag" value="Voeg een vraag toe"/>
				<input type="submit" class="button2" name="annuleer" value="Annuleer Quiz"/>
				<input type="submit" class="button3" name="enterQuiz" value="Maak Quiz aan"/>
				
		';
echo foot();
}

function createHTMLchange(){
	$categories = selectCategory();
	
echo head("Create Category");
echo '	<font class="text1" size="5">Selecteer categorie: </font>
	
		<!-- categorie -->
				<select name="categorie">
					  <option value="">Select categorie...</option>';
				
				foreach ($categories as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$categories'];
				echo '</select>
				
				<select name="newCategorie">
					  <option value="">Select categorie...</option>';
				
				foreach ($categories as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$newCategories'];
				echo '</select>
			
				<br>
				
		<!-- quiznaam -->
				<font class="text2" size="5">Quiznaam: </font>
				
				<select name="quiznaam">
					  <option value="">Select Quiznaam...</option>';
				
				foreach ($quiznaam as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$quiznaam'];
				echo '</select>
				
				<input type="text" class="tekstvak1" name="QuizNaam" placeholder="Quiz naam"/>
			
			</br>
			</br>
			
			
		<!-- vraag -->
				<font class="text3" size="5">Vraag: </font>
				
				<select name="vraagQuiz">
					  <option value="">Select vraag...</option>';
				
				foreach ($vraag as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$vraag'];
				echo '</select>
				
				<input type="text" class="tekstvak2" name="Vraag" placeholder="Vraag"/>
			<br>
			
		<!-- timer -->
				<font class="text13" size="5">Timer: </font>
				
				<select name="timer">
					  <option value="">Select timer...</option>';
				
				foreach ($timer as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$timer'];
				echo '</select>
				
				<input type="text" class="tekstvak8" name="timer" placeholder="Timer"/>
				
				
			</br>
			</br>
			
		<!-- antwoord 1 -->
				<font class="text4" size="5">Antwoord 1: </font>
				
				<select name="antwoord1">
					  <option value="">Select antwoord1...</option>';
				
				foreach ($antwoord1 as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$antwoord1'];
				echo '</select>
				
				<input type="text" class="tekstvak3" name="Antwoord1" placeholder="Antwoord"/>
				<input type=checkbox name="CorrectAntwoord1">
				<font class="text5" size="5">Correct antwoord</font>
				
			</br>
			
		<!-- antwoord 2 -->
				<font class="text6" size="5">Antwoord 2: </font>
				
				<select name="antwoord2">
					  <option value="">Select antwoord2...</option>';
				
				foreach ($antwoord2 as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$antwoord2'];
				echo '</select>
				
				<input type="text" class="tekstvak4" name="Antwoord2" placeholder="Antwoord"/>
				<input type=checkbox name="CorrectAntwoord2">
				<font class="text7" size="5">Correct antwoord</font>
			</br>
			
		<!-- antwoord 3 -->
				<font class="text8" size="5">Antwoord 3: </font>
				
				<select name="antwoord3">
					  <option value="">Select antwoord3...</option>';
				
				foreach ($antwoord3 as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$antwoord3'];
				echo '</select>
				
				<input type="text" class="tekstvak5" name="Antwoord3" placeholder="Antwoord"/>
				<input type=checkbox name="CorrectAntwoord3">
				<font class="text9" size="5">Correct antwoord</font>
			</br>
			
		<!-- antwoord 4 -->
				<font class="text10" size="5">Antwoord 4: </font>
				
				<select name="antwoord4">
					  <option value="">Select antwoord4...</option>';
				
				foreach ($antwoord4 as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$antwoord4'];
				echo '</select>
				
				<input type="text" class="tekstvak6" name="Antwoord4" placeholder="Antwoord"/>
				<input type=checkbox name="CorrectAntwoord4">
				<font class="text11" size="5">Correct antwoord</font>
			<br>
			<br>
			
		<!-- uitleg correct antwoord -->
				<font class="text12" size="5">Uitleg goed antwoord: </font>
				
				<select name="uitlegAntwoord">
					  <option value="">Select uitleg antwoord...</option>';
				
				foreach ($uitlegAntwoord as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$uitlegAntwoord'];
				echo '</select>
				
				<textarea type="" class="tekstvak7" name="uitlegGoedAntwoord" placeholder="Uitleg goed antwoord"></textarea>
			</br>
			</br>
				<input type="submit" class="button2" name="annuleer" value="Annuleer veranderen Quiz"/>
				<input type="submit" class="button3" name="changeQuiz" value="Verander Quiz"/>
		';
echo foot();
}

function createHTMLdelete(){
	$categories = selectCategory();
	
echo head("Create Category");
echo '	<font class="text1" size="5">Selecteer categorie: </font>
			<!-- categorie -->
				<select name="categorie">
					  <option value="">Select categorie...</option>';
				
				foreach ($categories as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$categories'];
				echo '</select>
			
				<br>
				
		<!-- quiznaam -->
				<font class="text2" size="5">Quiznaam: </font>
				
				<select name="quiznaam">
					  <option value="">Select Quiznaam...</option>';
				
				foreach ($quiznaam as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$quiznaam'];
				echo '</select>
			
			</br>
			</br>
			
			
		<!-- vraag -->
				<font class="text3" size="5">Vraag: </font>
				
				<select name="vraagQuiz">
					  <option value="">Select vraag...</option>';
				
				foreach ($vraag as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$vraag'];
				echo '</select>

			<br>
			
		<!-- timer -->
				<font class="text13" size="5">Timer: </font>
				
				<select name="timer">
					  <option value="">Select timer...</option>';
				
				foreach ($timer as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$timer'];
				echo '</select>

				
				
			</br>
			</br>
			
		<!-- antwoord 1 -->
				<font class="text4" size="5">Antwoord 1: </font>
				
				<select name="antwoord1">
					  <option value="">Select antwoord1...</option>';
				
				foreach ($antwoord1 as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$antwoord1'];
				echo '</select>
				
			</br>
			
		<!-- antwoord 2 -->
				<font class="text6" size="5">Antwoord 2: </font>
				
				<select name="antwoord2">
					  <option value="">Select antwoord2...</option>';
				
				foreach ($antwoord2 as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$antwoord2'];
				echo '</select>
			</br>
			
		<!-- antwoord 3 -->
				<font class="text8" size="5">Antwoord 3: </font>
				
				<select name="antwoord3">
					  <option value="">Select antwoord3...</option>';
				
				foreach ($antwoord3 as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$antwoord3'];
				echo '</select>
			</br>
			
		<!-- antwoord 4 -->
				<font class="text10" size="5">Antwoord 4: </font>
				
				<select name="antwoord4">
					  <option value="">Select antwoord4...</option>';
				
				foreach ($antwoord4 as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$antwoord4'];
				echo '</select>
			<br>
			<br>
			
		<!-- uitleg correct antwoord -->
				<font class="text12" size="5">Uitleg goed antwoord: </font>
				
				<select name="uitlegAntwoord">
					  <option value="">Select uitleg antwoord...</option>';
				
				foreach ($uitlegAntwoord as $x => $row){
				// vul drop down		
					echo "<option value=\"$row\">$row</option> </br>";
				};
				
					$selected = $_POST['$uitlegAntwoord'];
				echo '</select>

			</br>
			</br>
				<input type="submit" class="button2" name="annuleer" value="Annuleer verwijderen Quiz"/>
				<input type="submit" class="button3" name="verwijderQuiz" value="Verwijder Quiz"/>
		';
echo foot();
}

/* 
====================================================
  Main select functions
====================================================
*/

//function to select category
function selectCategory(){
	//echo "DEBUG: function selectCategory</br>";
	
	
	$handle = connectDB();
	//select all categories from DB
	$query = sprintf("SELECT * FROM categorie");
	$result = mysqli_query($handle, $query);
	//echo "DEBUG: after query</br>";
	
	$categories = array();
	
	while($row = mysqli_fetch_assoc($result)){
		//var_dump($row['categorie']);
		array_push($categories, $row['categorie']);
	}
	
	return $categories;
	
}

function selectIdQuiz(){
	//echo "DEBUG: function selectIdQuiz</br>";
	
	
	$handle = connectDB();
	//select all idQuizes from DB
	$query = sprintf("SELECT * FROM quiz");
	$result = mysqli_query($handle, $query);
	//echo "DEBUG: after query</br>";
	
	$idQuiz = array();
	
	while($row = mysqli_fetch_assoc($result)){
		//var_dump($row['categorie']);
		array_push($idQuiz, $row['nameQuiz']);
	}
	
	return $idQuiz;
}

/* 
====================================================
  Main vraag functions
====================================================
*/

//function to create new vraag
function createVraag($vraag){
	echo "dit is de nieuwe vraag: $vraag</br>";

	$handle = connectDB();
	
	$query = sprintf("SELECT c.idCategorie FROM categorie c, quiz q WHERE c.idCategorie = q.idCategorie AND c.categorie = '$select'");
		$result = mysqli_query($handle, $query);
		
		$array = array();
		$row=$result->fetch_assoc();
		$id = intval($row['idCategorie']);
	
	
	//query to insert new vraag into DB
	$query = sprintf("INSERT INTO vraag (idQuiz, vraag) VALUES ('$idQuizes','$vraag')");
	//$result = mysqli_query($handle , $query);

	echo $query;
	
	
	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new vraag $vraag gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

//function to change vraag
function changeVraag($vraag){
	echo "dit is de vraag: $vraag</br>";

	$handle = connectDB();
	
	//query to change vraag into DB
	$query = sprintf("INSERT INTO vraag (vraag) VALUES ('$vraag')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new vraag $vraag gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

//function to delete vraag
function deleteVraag($vraag){
	echo "dit is de nieuwe vraag: $vraag</br>";

	$handle = connectDB();
	
	//query to delete vraag into DB
	$query = sprintf("INSERT INTO vraag (vraag) VALUES ('$vraag')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new vraag $vraag gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

/* 
====================================================
  Main antwoord functions
====================================================
*/
//function to create new antwoorden
function createAntwoord($antwoord){
	echo "dit is de nieuwe antwoord: $antwoord</br>";

	$handle = connectDB();
	
	//query to insert new antwoord into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$antwoord')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new antwoord $antwoord gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

//function to change antwoorden
function chagneAntwoord($antwoord){
	echo "dit is de nieuwe antwoord: $antwoord</br>";

	$handle = connectDB();
	
	//query to change antwoord into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$antwoord')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new antwoord $antwoord gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

//function to delete antwoorden
function deleteAntwoord($antwoord){
	echo "dit is de nieuwe antwoord: $antwoord</br>";

	$handle = connectDB();
	
	//query to delete antwoord into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$antwoord')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new antwoord $antwoord gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}


/* 
====================================================
  Main quizNaam functions
====================================================
*/

//function to create new quizNaam
function createQuizNaam($quizNaam){
	echo "dit is de nieuwe quizNaam: $quizNaam</br>";

	$handle = connectDB();
/*
	||||| query moet nog worden gemaakt |||||
*/	
	//query to insert new quizNaam into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$quizNaam')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new quizNaam $quizNaam gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

//function to change quizNaam
function chagneQuizNaam($quizNaam){
	echo "dit is de nieuwe quizNaam: $quizNaam</br>";

	$handle = connectDB();
/*
	||||| query moet nog worden gemaakt |||||
*/	
	//query to change quizNaam into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$quizNaam')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new quizNaam $quizNaam gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");


}

//function to delete quizNaam
function deleteQuizNaam($quizNaam){
	echo "dit is de nieuwe quizNaam: $quizNaam</br>";

	$handle = connectDB();
/*
	||||| query moet nog worden gemaakt |||||
*/	
	//query to delete quizNaam into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$quizNaam')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new quizNaam $quizNaam gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

/* 
====================================================
  Main timer functions
====================================================
*/



/* 
====================================================
  Main uitleg goed antwoord functions
====================================================
*/

//function to create goed antwoord
function createGoedAntwoord($goedAntwoord){
	echo "dit is de nieuwe goedAntwoord: $goedAntwoord</br>";

	$handle = connectDB();
/*
	||||| query moet nog worden gemaakt |||||
*/	
	//query to insert new goedAntwoord into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$goedAntwoord')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new goedAntwoord $goedAntwoord gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

//function to change goed antwoord
function changeGoedAntwoord($goedAntwoord){
	echo "dit is de nieuwe goedAntwoord: $goedAntwoord</br>";

	$handle = connectDB();
/*
	||||| query moet nog worden gemaakt |||||
*/	
	//query to change goedAntwoord into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$goedAntwoord')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new goedAntwoord $goedAntwoord gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

//function to delete goed antwoord
function deleteGoedAntwoord($goedAntwoord){
	echo "dit is de nieuwe goedAntwoord: $goedAntwoord</br>";

	$handle = connectDB();
/*
	||||| query moet nog worden gemaakt |||||
*/	
	//query to delete goedAntwoord into DB
	$query = sprintf("INSERT INTO antwoord (antwoord) VALUES ('$goedAntwoord')");
	$result = mysqli_query($handle , $query);

	/*
	var_dump($query);
	var_dump($result);
	*/
	
	if ($result){
		echo "DEBUG: Opslaan new goedAntwoord $goedAntwoord gelukt";
	} else {
		echo "ERROR: opslaan in DB niet gelukt";
	}
	
	// terug naar homeQuizLeraar.php
	//returnToHTML("Location: aanmakenQuiz.php");

}

?>