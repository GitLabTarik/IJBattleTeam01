<!--
Auteur: Cihad Palta, IB101, 5007291417
Project Agile Development team IJB101
Dit bestand is geschreven om nieuwe vragen in te voeren. De gegevens worden opgeslagen in de database.
-->

<?php
	session_start();
   function renderFormVraag($idQuiz, $vraag, $uitleg, $error) {
   ?>
<html>
   <title>Nieuwe vraag invoeren</title>
   <body>
   
   	<link rel="stylesheet" type="text/css" href="formStyle.css" />
   
      <?php
         if ($error != '') // IF there are any errors, display them
             {
             echo '<div style="padding:4px; border:1px solid red; color:red;">' . $error . '</div>';
         }
         
         ?>
   </body>
   <form action="" method="post" class="form-style">
      <div>
         <label>idQuiz: *</label>		 
		 
		 <?php	
		include('connect-db.php');		 
		$sql = "SELECT * FROM quiz";
		$result = mysqli_query($conn, $sql);

		echo '<select name="idQuiz">';
		while ($row = mysqli_fetch_array($result)) {
			echo "<option value='" . $row['nameQuiz'] . "'>" . $row['nameQuiz'] . "</option>";
		}
		echo "</select>";
        ?>
		
		 
		 
		 
		 
		 
		 
         <label>Vraag: *</label>
         <input type="text" name="vraag">
         <label>Uitleg: *</label>
         <input type="text" name="uitleg">
         <p>* required</p>
         <input type="submit" name="submit" value="Opslaan">
      </div>
   </form>
</html>
<?php
   }
   include('connect-db.php'); // Connect to the database
   
   if (isset($_POST['submit'])) // Checks if the submit button has been pressed
       { // Gets the data filled in the form and making sure its valid
       $idQuiz	     = mysqli_real_escape_string($conn, $_POST['idQuiz']);
       $vraag       = mysqli_real_escape_string($conn, $_POST['vraag']);
       $uitleg      = mysqli_real_escape_string($conn, $_POST['uitleg']);
   
       
       // IF statement to check everything is filled in
       if ($idQuiz == '' || $vraag == '' || $uitleg == '') {
           
           $error = 'Vul a.u.b. alle gegevens in!'; // Error message if some field(s) are not filled in
           renderFormVraag($idQuiz, $vraag, $uitleg, $error); // Show form again to continue filling in
       } else // IF everything is filled in
           {
			   
			   $sql = "select q.idQuiz FROM quiz q where q.nameQuiz = '$idQuiz'";
				$result = mysqli_query($conn, $sql);
				  $row=$result->fetch_assoc();
		$id = intval($row['idQuiz']);
		echo $id; 
						   
           $query = mysqli_query($conn, "INSERT vraag SET idQuiz=$id, vraag='$vraag', uitleg='$uitleg'"); //or die(mysql_error()
           $tekst = "quizBeheer-container";
			$_SESSION['container'] = $tekst;
		   header("Location: controlpanel.php"); // Once saved, redirect back to vraagWeergeven.php
       }
   } else // IF nothing is submitted just display an empty form
       {
       renderFormVraag('', '', '', '');
   }
   ?>
  