<!--
Auteur: Cihad Palta, IB101, 5007291417
Project Agile Development team IJB101
Dit bestand is geschreven om gegevens van docenten te bewerken. De gegevens worden bewerkt in de database.
-->

<?php
session_start();
function renderForm($id, $firstname, $lastname, $password, $idStamgroep, $deelschool, $email, $error)
// Create a function to display the form
{
?>
<html>
   <head>
      <title>Docent bewerken</title>
   </head>
   <body>
   
   	<link rel="stylesheet" type="text/css" href="formStyle.css" />
	
      <?php
         if ($error != '') // If there are any errors, display them
             {
             echo '<div style="padding:4px; border:1px solid red; color:red;">' . $error . '</div>';
         }
         ?>
      <form action="" method="post" class="form-style">
         <input type="hidden" name="id" value="<?php echo $id; ?>" />
         <div>
            <p><label>ID:</label>
               <?php echo $id; ?>
            </p>
            <label>Voornaam: *</label> <input type="text" name="firstname" value="<?php echo $firstname; ?>" /><br/>
            <label>Achternaam: *</label> <input type="text" name="lastname" value="<?php echo $lastname; ?>" /><br/>
            <label>Wachtwoord: *</label> <input type="password" name="password" value="<?php echo $password; ?>" /><br/><br/>
            <label>Stamgroep: *</label>
               
			   <?php	
				include('connect-db.php');		 
				$sql = "SELECT * FROM stamgroep";
				$result = mysqli_query($conn, $sql);

				echo '<select name="stamgroep">';
				while ($row = mysqli_fetch_array($result)) {
					echo "<option value='" . $row['naamStamgroep'] . "'>" . $row['naamStamgroep'] . "</option>";
				}
				echo "</select>";
				?>

            <label>Deelschool: *</label>
            <select name="deelschool">
               <option value="IJburg1">IJburg1</option>
               <option value="IJburg2">IJburg2</option>
            </select> <br/>
            <label>E-Mail: *</label> <input type="email" name="email" value="<?php echo $email; ?>" /> <br/>
            <p>* Required</p>
            <input type="submit" name="submit" value="Submit">
         </div>
      </form>
   </body>
</html>
<?php
}
include('connect-db.php'); // Connect to the database

if (isset($_POST['submit'])) { // Check to confirm the form has been submitted
    
    if (is_numeric($_POST['id'])) { // Check to confirm the id
        
        $id         = $_POST['id']; // Get all form data and make sure it's valid
        $firstname  = mysqli_real_escape_string($conn, $_POST['firstname']);
        $lastname   = mysqli_real_escape_string($conn, $_POST['lastname']);
        $password   = mysqli_real_escape_string($conn, $_POST['password']);
        $stamgroep  = mysqli_real_escape_string($conn, $_POST['stamgroep']);
        $deelschool = mysqli_real_escape_string($conn, $_POST['deelschool']);
        $email      = mysqli_real_escape_string($conn, $_POST['email']);
        
        // IF statement to check everything is filled in
        if ($firstname == '' || $lastname == '' || $password == '' || $stamgroep == '' || $deelschool == '' || $email == '') {
            
            $error = 'Vul a.u.b. alle gegevens in!'; // Error message if some field(s) are not filled in
            
            renderForm($id, $firstname, $lastname, $password, $stamgroep, $deelschool, $email, $error); // Show form again to continue filling in
        } else // IF everything is filled in
            {
				$sql = "select s.idStamgroep FROM stamgroep s where s.naamStamgroep = '$stamgroep'";
				$result = mysqli_query($conn, $sql);
				  $row=$result->fetch_assoc();
			$id1 = intval($row['idStamgroep']);
				
            // Insert query with the filled in variables
            mysqli_query($conn, "UPDATE docent SET voornaam='$firstname', achternaam='$lastname', wachtwoord='$password', idStamgroep=$id1, deelschool='$deelschool', email='$email' WHERE idDocent=$id");
			//or die(mysql_error());
             $tekst = "docentBeheer-container";
			$_SESSION['container'] = $tekst;
            header("Location: controlpanel.php");  // Once saved, redirect back to the docentWeergeven.php page
        }
    } else {
        echo 'Deze gebruiker bestaat niet (meer)!'; // If the ID isn't valid show an error
    }
} else // Before the edit form is submitted, get all the data from the row selected in the database so it can be edited
    {
    
    // get the 'id' value from the URL (if it exists), making sure that it is valid (checing that it is numeric/larger than 0)
    if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0) {
        // query db
        $id  = $_GET['id'];
        $sql = "SELECT * FROM docent WHERE idDocent=$id";
        $result = mysqli_query($conn, $sql) or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        
        // check that the 'id' matches up with a row in the databse
        if ($row) {
            
            // get data from db
            $firstname   = $row['voornaam'];
            $lastname    = $row['achternaam'];
            $password    = $row['wachtwoord'];
            $idStamgroep = $row['idStamgroep'];
            $deelschool  = $row['deelschool'];
            $email       = $row['email'];
            
            
            // show form
            renderForm($id, $firstname, $lastname, $password, $idStamgroep, $deelschool, $email, '');
        } else
        // if no match, display result
            {
            echo "No results!";
        }
    } else
    // if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error
        {
        echo 'Error!';
    }
}
?>