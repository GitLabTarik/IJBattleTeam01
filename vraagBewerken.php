<!--
Auteur: Cihad Palta, IB101, 5007291417
Project Agile Development team IJB101
Dit bestand is geschreven om vragen te bewerken. De gegevens worden bewerkt in de database.
-->

<?php
	session_start();
   function renderForm($id, $idQuiz, $vraag, $uitleg, $error)
   // Create a function to display the form
   {
   ?>
<html>
   <head>
      <title>Vraag bewerken</title>
   </head>
   <body>
   
   	<link rel="stylesheet" type="text/css" href="formStyle.css" />
   
      <?php
         if ($error != '') // If there are any errors, display them
             {
             echo '<div style="padding:4px; border:1px solid red; color:red;">' . $error . '</div>';
         }
         ?>
		 
      <form action="" method="post" class="form-style">
         <input type="hidden" name="id" value="<?php echo $id; ?>" />
         <div>
            <p><label>ID:</label>
               <?php echo $id; ?>
            </p>
            <label>quiznaam: *</label>
             <?php	
				include('connect-db.php');		 
				$sql = "SELECT * FROM quiz";
				$result = mysqli_query($conn, $sql);

				echo '<select name="idQuiz">';
				while ($row = mysqli_fetch_array($result)) {
					echo "<option value='" . $row['nameQuiz'] . "'>" . $row['nameQuiz'] . "</option>";
				}
				echo "</select>";
				?>
            <br/>
            <label>Vraag: *</label> <input type="text" name="vraag" value="<?php echo $vraag; ?>" /> <br/>
            T
            <label>Uitleg: *</label> <input type="text" name="uitleg" value="<?php echo $uitleg; ?>" /> <br/>
            <p>* Required</p>
            <input type="submit" name="submit" value="Submit">
         </div>
      </form>
   </body>
</html>
<?php

   }
   include('connect-db.php'); // Connect to the database
   
   if (isset($_POST['submit'])) { // Check to confirm the form has been submitted
       
       if (is_numeric($_POST['id'])) { // Check to confirm the id
           
           $id         = $_POST['id']; // Get all form data and make sure it's valid
           $idQuiz  = mysqli_real_escape_string($conn, $_POST['idQuiz']);
           $vraag   = mysqli_real_escape_string($conn, $_POST['vraag']);
           $uitleg   = mysqli_real_escape_string($conn, $_POST['uitleg']);
           
           // IF statement to check everything is filled in
           if ($vraag == '' || $uitleg == '') {
               
               $error = 'Vul a.u.b. alle gegevens in!'; // Error message if some field(s) are not filled in
               
               renderForm($id, $vraag, $uitleg, $error); // Show form again to continue filling in
           } else // IF everything is filled in
               {
				   $sql = "select q.idQuiz FROM quiz q where q.nameQuiz = '$idQuiz'";
				$result = mysqli_query($conn, $sql);
				  $row=$result->fetch_assoc();
			$id1 = intval($row['idQuiz']);
				   
               // Insert query with the filled in variables
               mysqli_query($conn, "UPDATE vraag SET idQuiz=$id1, vraag='$vraag', uitleg='$uitleg' WHERE idVraag=$id"); //or die(mysql_error());
               $tekst = "quizBeheer-container";
			   $_SESSION['container'] = $tekst;
               header("Location: controlpanel.php"); // Once saved, redirect back to the vraagWeergeven.php page
           }
       } else {
           echo 'Deze vraag bestaat niet (meer)!'; // If the ID isn't valid show an error
       }
   } else // Before the edit form is submitted, get all the data from the row selected in the database so it can be edited
       {
       
       // get the 'id' value from the URL (if it exists), making sure that it is valid (checing that it is numeric/larger than 0)
       if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0) {
           // query db
           $id  = $_GET['id'];
           $sql = "SELECT * FROM vraag WHERE idVraag=$id";
           $result = mysqli_query($conn, $sql); //or die(mysql_error());
           $row = mysqli_fetch_assoc($result);
           
           // check that the 'id' matches up with a row in the databse
           if ($row) {
               
               // get data from db
               $idQuiz = $row['idQuiz'];
   			$vraag   = $row['vraag'];
               $uitleg    = $row['uitleg'];
   			
               // show form
               renderForm($id, $idQuiz, $vraag, $uitleg, '');
           } else
           // if no match, display result
               {
               echo "No results!";
           }
       } else
       // if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error
           {
           echo 'Error!';
       }
   }
   ?>