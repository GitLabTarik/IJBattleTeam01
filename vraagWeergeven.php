<!--
Auteur: Cihad Palta, IB101, 5007291417
Project Agile Development team IJB101
Dit bestand is geschreven om vragen te weergeven in een tabel. De gegevens worden geselecteerd uit de database.
-->

<html>
   <head>
      <title>Vragen overzicht</title>
      <style>
         html * {
         font-family: Arial !important;
         }
         table {
         border-collapse: collapse;
         width: 100%;
         }
         th,
         td {
         text-align: left;
         padding: 8px;
         }
         tr:nth-child(even) {
         background-color: #f2f2f2
         }
         th {
         background-color: #483D8B;
         color: white;
         }
      </style>
   </head>
   <body>
      <?php
         include('connect-db.php'); // Connect to DB
         
         $sql    = "SELECT * FROM vraag"; // Query to get all data from table
         $result = mysqli_query($conn, $sql); // Define variable result with the results from the query
         
         echo "<table border='1' cellpadding='10'>"; // Create the table
         echo "<tr> <th>idVraag</th> <th>idQuiz</th> <th>Vraag</th> <th>Score</th>
             <th>Uitleg</th> </tr>"; // Table headers
         
		 
         if (mysqli_num_rows($result) > 0) { // Whileloop through results of database query, displaying them in the table
             
             while ($row = mysqli_fetch_assoc($result)) { // Whileloop through results of database query, displaying them in the table
                 
                 echo "<tr>"; // Print the data in the table rows
                 echo '<td>' . $row['idVraag'] . '</td>';
                 echo '<td>' . $row['idQuiz'] . '</td>';
                 echo '<td>' . $row['vraag'] . '</td>';
                 echo '<td>' . $row['score'] . '</td>';
                 echo '<td>' . $row['uitleg'] . '</td>';
                 
                 echo '<td><a href="vraagBewerken.php?id=' . $row['idVraag'] . '">Bewerken</a></td>';
                 echo '<td><a href="vraagVerwijderen.php?id=' . $row['idVraag'] . '">Verwijderen</a></td>';
                 echo "</tr>";
             }
             
         } else { // If there are no results echo no results
             
             echo "Er zijn geen vragen gevonden";
             
         }
         
         echo "</table>"; // Close table
         
         ?>
      <p><a href="vraagNieuw.php">Nieuwe vraag toevoegen</a></p>
   </body>
</html>