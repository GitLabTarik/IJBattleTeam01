<!--
Auteur: Cihad Palta, IB101, 5007291417
Project Agile Development team IJB101
Dit bestand is geschreven om gegevens van leerlingen te bewerken. De gegevens worden bewerkt in de database.
-->

<?php
session_start(); 
function renderForm($id, $firstname, $lastname, $password, $idStamgroep, $deelschool, $email, $error) 
// Create a function to display the form
{
?>
<html>
   <head>
      <title>Leerling bewerken</title>
   </head>
   <body>
   
   	<link rel="stylesheet" type="text/css" href="formStyle.css" />
   
      <?php
         if ($error != '') // If there are any errors, display them
             {
             echo '<div style="padding:4px; border:1px solid red; color:red;">' . $error . '</div>';
         }
         ?> 
      <form action="" method="post" class="form-style">
         <input type="hidden" name="id" value="<?php echo $id; ?>"/>
         <div>
			<h1> Bewerk de gegevens van uw leerling </h1>
            <label>Voornaam: *</label> <input type="text" name="firstname" value="<?php echo $firstname; ?>"/><br/>
            <label>Achternaam: *</label> <input type="text" name="lastname" value="<?php echo $lastname; ?>"/><br/>
            <label>Wachtwoord: *</label> <input type="password" name="password" value="<?php echo $password; ?>"/><br/><br/>
            <label>Stamgroep: *</label>

			<?php	
				include('connect-db.php');		 
				$sql = "SELECT * FROM stamgroep";
				$result = mysqli_query($conn, $sql);

				echo '<select name="stamgroep">';
				while ($row = mysqli_fetch_array($result)) {
					echo "<option value='" . $row['naamStamgroep'] . "'>" . $row['naamStamgroep'] . "</option>";
				}
				echo "</select>";
			?>
            <br/>
            <label>Deelschool: *</label> 
            <select name="deelschool">
               <option value="IJburg1">IJburg1</option>
               <option value="IJburg2">IJburg2</option>
            </select>
            <br/>
            <label>E-Mail: *</label> <input type="email" name="email" value="<?php echo $email; ?>"/><br/>
            <p>* Required</p>
            <input type="submit" name="submit" value="Submit">
         </div>
      </form>
   </body>
</html> 
 <?php
}
include('connect-db.php'); // Connect to the database

if (isset($_POST['submit'])) {
    
    if (is_numeric($_POST['id'])) {
        
        // get form data, making sure it is valid
        $id         = $_POST['id'];
        $firstname  = mysqli_real_escape_string($conn, $_POST['firstname']);
        $lastname   = mysqli_real_escape_string($conn, $_POST['lastname']);
        $password   = mysqli_real_escape_string($conn, $_POST['password']);
        $stamgroep  = mysqli_real_escape_string($conn, $_POST['stamgroep']);
        $deelschool = mysqli_real_escape_string($conn, $_POST['deelschool']);
        $email      = mysqli_real_escape_string($conn, $_POST['email']);
        
        // check that firstname/lastname fields are both filled in
        if ($firstname == '' || $lastname == '') {
            // generate error message
            $error = 'ERROR: Please fill in all required fields!';
            
            //error, display form
            renderForm($id, $firstname, $lastname, $password, $stamgroep, $deelschool, $email, $error);
        } else {
			$sql = "select s.idStamgroep FROM stamgroep s where s.naamStamgroep = '$stamgroep'";
				$result = mysqli_query($conn, $sql);
				  $row=$result->fetch_assoc();
			$id1 = intval($row['idStamgroep']);
			
            // save the data to the database
            mysqli_query($conn, "UPDATE leerling SET voornaam='$firstname', achternaam='$lastname', wachtwoord='$password', idStamgroep=$id1, deelschool='$deelschool', email='$email' WHERE idLeerling=$id"); //or die(mysql_error());
            
            // once saved, redirect back to the view page

$tekst = "leerlingBeheer-container";
$_SESSION['container'] = $tekst;

            header("Location: controlpanel.php");
        }
    } else {
        // if the 'id' isn't valid, display an error
        echo 'Error!';
    }
} else
// if the form hasn't been submitted, get the data from the db and display the form
    {
    
    // get the 'id' value from the URL (if it exists), making sure that it is valid (checing that it is numeric/larger than 0)
    if (isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0) {
        // query db
        $id  = $_GET['id'];
        $sql = "SELECT * FROM leerling WHERE idLeerling=$id";
        $result = mysqli_query($conn, $sql); //or die(mysql_error());
        $row = mysqli_fetch_assoc($result);
        
        // check that the 'id' matches up with a row in the databse
        if ($row) {
            
            // get data from db
            $firstname  = $row['voornaam'];
            $lastname   = $row['achternaam'];
            $password   = $row['wachtwoord'];
            $stamgroep  = $row['idStamgroep'];
            $deelschool = $row['deelschool'];
            $email      = $row['email'];
            
            
            // show form
            renderForm($id, $firstname, $lastname, $password, $stamgroep, $deelschool, $email, '');
        } else
        // if no match, display result
            {
            echo "No results!";
        }
    } else
    // if the 'id' in the URL isn't valid, or if there is no 'id' value, display an error
        {
        echo 'Error!';
    }
}
?>