<?php

//select function by button and connect php homeQuizLeraarScript with html homeQuizLeraar
if (isset($_POST['button_newVraag&antwoord'])){
	createHTMLnewVraag_antwoord();
} else if (isset($_POST['button_changeVraag&antwoord'])){
	createHTMLchangeVraag_antwoord();
} else if (isset($_POST['button_deleteVraag&antwoord'])){
	createHTMLdeleteVraag_antwoord();
} else if(isset($_POST['submit_Vraag_Antwoord'])){
	$newVraag = $_POST['submit_newVraag'];
	$newAntwoord1 = $_POST['submit_newAntwoord1'];
	$newAntwoord2 = $_POST['submit_newAntwoord2'];
	$newAntwoord3 = $_POST['submit_newAntwoord3'];
	$newAntwoord4 = $_POST['submit_newAntwoord4'];
	$goedAntwoord = $_POST['submit_goedAntwoord'];
	createVraag($newVraag);
	createAntwoord($newAntwoord1, $newAntwoord2, $newAntwoord3, $newAntwoord4, $goedAntwoord);
}

/* 
====================================================
  Helper functions
====================================================
*/

//back to hmtl homeQuizLeraar page
function returnToHTML($url){
	//sleep(3);
	header($url);
	exit();

}


//html generate functions
function head($title)
{
   $title = htmlentities($title);
   return <<<END
<html>
<head><title>$title</title></head>
<body>
<!-- anything else that goes before dynamic content -->
END;
}

function foot()
{
   return <<<END
<!-- anything that comes after the dynamic content -->
</body>
</html>
END;
} 


//function to connect to mySQL DB
function connectDB(){
	$username = "root";
	$password = "admin";
	$hostname = "localhost:3306"; 
	$dbname = "ijbattle";

	//connection to the database
	$dbhandle = mysqli_connect($hostname, $username, $password, $dbname) 
	  or die("Unable to connect to MySQL");
	//echo "DEBUG: Connected to MySQL<br>";
	return $dbhandle;
}

/* 
====================================================
  Create HTML functions
====================================================
*/

function createHTMLnewVraag_antwoord(){
	echo "DEBUG: after createHTMLnewVraag_antwoord fucntion";
	
	$idQuiz = selectIdQuiz();
	$goedAntwoord = selectGoedAntwoord();
	
	echo head("Create vraag en antwoord");
	echo '<form method="post" action="homeQuizLeraarVraag&AntwoordScript.php">
					<div class="text1">
						<font size="10"><b>voer nieuwe vraag en antwoorden in: </b></font><br><br>
					</div>
					<font size="5"><b>Vraag</b></font><br>
					<input type="text" name="submit_newVraag" placeholder="New vraag"/><br><br>
					<font size="5"><b>Select quiz id</b></font><br>
					
					<select name="idQuiz">
					  <option value="">Select quiz id...</option>';
					  
	foreach ($idQuiz as $x => $row){
		// vul drop down		
			echo "<option value=\"$row\">$row</option> </br>";
	};
	
		$selected = $_POST['$idQuiz'];
	
	echo '			</select><br><br>
					<font size="5"><b>Antwoorden</b></font>
					<font size="5"><b>Goed antwoord</b></font><br>
					<input type="text" name="submit_newAntwoord1" placeholder="New antwoord"/>
					<select name="goedAntwoord">
					  <option value="">Select goedAntwoord...</option>';
					  
	foreach ($goedAntwoord as $x => $row){
		// vul drop down		
			echo "<option value=\"$row\">$row</option> </br>";
	};
	
		$selected = $_POST['$goedAntwoord'];
		
	echo '			</select>
					<br>
					
					<input type="text" name="submit_newAntwoord2" placeholder="New antwoord"/>
					<select name="goedAntwoord">
					  <option value="">Select goedAntwoord...</option>';
					  
	foreach ($goedAntwoord as $x => $row){
		// vul drop down		
			echo "<option value=\"$row\">$row</option> </br>";
	};
	
		$selected = $_POST['$goedAntwoord'];
		
	echo '			</select>
					<br>
					
					<input type="text" name="submit_newAntwoord3" placeholder="New antwoord"/>
					<select name="goedAntwoord">
					  <option value="">Select goedAntwoord...</option>';
					  
	foreach ($goedAntwoord as $x => $row){
		// vul drop down		
			echo "<option value=\"$row\">$row</option> </br>";
	};
	
		$selected = $_POST['$goedAntwoord'];
		
	echo '			</select>
					<br>
					
					<input type="text" name="submit_newAntwoord4" placeholder="New antwoord"/>
					<select name="goedAntwoord">
					  <option value="">Select goedAntwoord...</option>';
					  
	foreach ($goedAntwoord as $x => $row){
		// vul drop down		
			echo "<option value=\"$row\">$row</option> </br>";
	};
	
		$selected = $_POST['$goedAntwoord'];
		
	echo '			</select>					
					
					<br><br>					
					
					<input type="submit" name="submit_Vraag_Antwoord" value="Enter"/>
					<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>
				</form>';
	echo foot();
}

function createHTMLchangeVraag_antwoord(){
	echo "DEBUG: after createHTMLchangeVraag_antwoord fucntion";
	
	$idQuiz = selectIdQuiz();
	$new_idQuiz = selectIdQuiz();
	
	echo head("change vraag en antwoord");
	echo '<form method="post" action="homeQuizLeraarVraag&AntwoordScript.php">
					<div class="text1">
						<font size="10"><b>verander de vraag en antwoorden in: </b></font><br><br>
					</div>
					<font size="5"><b>Vraag</b></font><br>
					
					<select name="">
						<option value=""></option>
					</select>
					
					<input type="text" name="submit_newVraag" placeholder="New vraag"/><br><br>
					<font size="5"><b>Select quiz id</b></font><br>
					
					<select name="idQuiz">
					  <option value="">Select quiz id...</option>';
					  
	foreach ($idQuiz as $x => $row){
		// vul drop down		
			echo "<option value=\"$row\">$row</option> </br>";
	};
	
		$selected = $_POST['$idQuiz'];
		
	echo '			</select>
					<select name="New_idQuiz">
					  <option value="">Select new quiz id...</option>';
					  
	foreach ($new_idQuiz as $x => $row){
		// vul drop down		
			echo "<option value=\"$row\">$row</option> </br>";
	};

		$new_selected = $_POST['$new_idQuiz'];
	
	echo '			</select><br><br>
					<font size="5"><b>Antwoorden</b></font><br>
					
					<select name="">
						<option value=""></option>
					</select>
					<input type="text" name="submit_newAntwoord1" placeholder="New antwoord"/><br>
					
					<select name="">
						<option value=""></option>
					</select>
					<input type="text" name="submit_newAntwoord2" placeholder="New antwoord"/><br>
					
					<select name="">
						<option value=""></option>
					</select>
					<input type="text" name="submit_newAntwoord3" placeholder="New antwoord"/><br>
					<select name="">
						<option value=""></option>
					</select>
					
					<input type="text" name="submit_newAntwoord4" placeholder="New antwoord"/><br><br>
					<font size="5"><b>Goed antwoord</b></font><br>
					
					<!-- goed antwoord drop down 2x -->
					<select name="">
						<option value=""></option>
					</select>
					
					<select name="">
						<option value=""></option>
					</select>
					<br><br>
					
					<input type="submit" name="submit_Vraag_Antwoord" value="Enter"/>
					<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>
				</form>';
	echo foot();
}

function createHTMLdeleteVraag_antwoord(){
	echo "DEBUG: after createHTMLdeleteVraag_antwoord fucntion";
	
	
	$idQuiz = selectIdQuiz();
	
	
	echo head("delete vraag en antwoord");
	echo '<form method="post" action="homeQuizLeraarVraag&AntwoordScript.php">
					<div class="text1">
						<font size="10"><b>verander de vraag en antwoorden in: </b></font><br><br>
					</div>
					<font size="5"><b>Vraag</b></font><br>
					
					<select name="">
						<option value=""></option>
					</select>
					<br><br>
					
					<font size="5"><b>Select quiz id</b></font><br>
					
					<select name="idQuiz">
					  <option value="">Select quiz id...</option>';
					  
	foreach ($idQuiz as $x => $row){
		// vul drop down		
			echo "<option value=\"$row\">$row</option> </br>";
	};
	
		$selected = $_POST['$idQuiz'];
	
	echo '			</select><br><br>
					<font size="5"><b>Antwoorden</b></font><br>
					
					<!-- goed antwoord1 drop down -->
					<select name="">
						<option value=""></option>
					</select>
					<br><br>
					
					<font size="5"><b>Goed antwoord</b></font><br>
					
					<!-- goed antwoord drop down -->
					<select name="">
						<option value=""></option>
					</select>
					<br><br>
					
					<input type="submit" name="submit_Vraag_Antwoord" value="Enter"/>
					<input type="submit" name="back_to_homeQuizLeraar" value="Annuleer"/>
				</form>';
	echo foot();
}

/* 
====================================================
  Main functions
====================================================
*/

function selectIdQuiz(){
	//echo "DEBUG: function selectQuiz</br>";
	
	$handle = connectDB();
	//select all categories from DB
	$query = sprintf("SELECT idQuiz FROM quiz");
	$result = mysqli_query($handle, $query);
	//echo "DEBUG: after query</br>";
	
	$idQuiz = array();

	
	while($row = mysqli_fetch_assoc($result)){
		//$var1 = $row['idQuiz', 'nameQuiz', 'idCategorie', 'status'];
		array_push($idQuiz, $row['idQuiz']);
	}
	
	return $idQuiz;
}

function selectGoedAntwoord(){
	//echo "DEBUG: funtion selectGoedAntwoord"
}

function createVraag($select, $newVraag){

echo "DEBUG: Na createVraag fucntion";
	$handle = connectDB();

	$query = sprintf("INSERT INTO vraag (idQuiz, vraag) VALUES ('$select', '$newVraag')");
	$result = mysqli_query($handle , $query);

	echo "DEBUG: after query";
	
	// terug naar homeQuizLeraarVraag&Antwoord.php
	//returnToHTML("Location: homeQuizLeraarVraag&Antwoord.php");
	
}

function createAntwoord($antwoord1, $antwoord2, $antwoord3, $antwoord4, $goedAntwoord){
	
	$handle = connectDB();
	$query = sprintf("INSERT INTO antwoord (antwoord, goedAntwoord) values('','','','','')");
	$result = mysqli_query($handle, $query);
	
}

?>