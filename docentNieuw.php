<!--
Auteur: Cihad Palta, IB101, 5007291417
Project Agile Development team IJB101
Dit bestand is geschreven om nieuwe docenten in te voeren. De gegevens worden opgeslagen in de database.
-->
<?php
	session_start();
   function renderForm($firstname, $lastname, $password, $idStamgroep, $deelschool, $email, $error)
   // Create a function to display the form
   {
   ?>
<html>
   <head>
      <title>Nieuwe docent invoeren</title>
   </head>
   <body>
   
   	<link rel="stylesheet" type="text/css" href="formStyle.css" />
   
      <?php
         if ($error != '') // IF there are any errors, display them
             {
             echo '<div style="padding:4px; border:1px solid red; color:red;">' . $error . '</div>';
         }
         
         ?>
      <form action="" method="post" class="form-style">
         <div>
            <label>Voornaam: *</label> <input type="text" name="firstname" value="<?php echo $firstname; ?>" /><br/>
            <label>Achternaam: *</label> <input type="text" name="lastname" value="<?php echo $lastname; ?>" /><br/>
            <label>Wachtwoord: *</label> <input type="password" name="password" value="<?php echo $password; ?>" /><br/>
            <label>Stamgroep: *</label>
            <?php	
				include('connect-db.php');		 
				$sql = "SELECT * FROM stamgroep";
				$result = mysqli_query($conn, $sql);

				echo '<select name="stamgroep">';
				while ($row = mysqli_fetch_array($result)) {
					echo "<option value='" . $row['naamStamgroep'] . "'>" . $row['naamStamgroep'] . "</option>";
				}
				echo "</select>";
			?>
            <label>Deelschool: *</label>
            <select name="deelschool">
               <option value="">Selecteer deelschool</option>
               <option value="IJburg1">IJburg1</option>
               <option value="IJburg2">IJburg2</option>
            </select> <br/>
            <label>E-Mail: *</label> <input type="email" name="email" value="<?php echo $email; ?>" /><br/>
            <p>* required</p>
            <input type="submit" name="submit" value="Opslaan">
         </div>
      </form>
   </body>
</html>

<?php
   }
   
   include('connect-db.php'); // Connect to the database
   
   if (isset($_POST['submit'])) // Checks if the submit button has been pressed
       { // Gets the data filled in the form and making sure its valid
       $firstname   = mysqli_real_escape_string($conn, $_POST['firstname']);
       $lastname    = mysqli_real_escape_string($conn, $_POST['lastname']);
       $password    = mysqli_real_escape_string($conn, $_POST['password']);
       $stamgroep = mysqli_real_escape_string($conn, $_POST['stamgroep']);
       $deelschool  = mysqli_real_escape_string($conn, $_POST['deelschool']);
       $email       = mysqli_real_escape_string($conn, $_POST['email']);
       
       // IF statement to check everything is filled in
       if ($firstname == '' || $lastname == '' || $password == '' || $stamgroep == '' || $deelschool == '' || $email == '') {
           
           $error = 'Vul a.u.b. alle gegevens in!'; // Error message if some field(s) are not filled in
           
           renderForm($firstname, $lastname, $password, $stamgroep, $deelschool, $email, $error); // Show form again to continue filling in
       } else // IF everything is filled in
           {
			   
			   $sql = "select s.idStamgroep FROM stamgroep s where s.naamStamgroep = '$stamgroep'";
				$result = mysqli_query($conn, $sql);
				  $row=$result->fetch_assoc();
			$id1 = intval($row['idStamgroep']);
			
           // Insert query with the filled in variables
		   
           mysqli_query($conn, "INSERT docent SET voornaam='$firstname', achternaam='$lastname', wachtwoord='$password', idStamgroep=$id1, deelschool='$deelschool', email='$email'"); //or die(mysql_error());
		
			$tekst = "docentBeheer-container";
			$_SESSION['container'] = $tekst;
            header("Location: controlpanel.php"); // Once saved, redirect back to docentWeergeven.php
       }
   } else // IF nothing is submitted just display an empty form
       {
       renderForm('', '', '', '', '', '', '');
   }
   ?>