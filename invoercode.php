<!-- Author: Team IJB01 Bryan Loos
Laatste edit: 12-04-16 -->
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="mainStyleSheet.css" />
		<link rel="stylesheet" type="text/css" href="invoercodeStyleSheet.css" />
		<title> IJBattle Invoer Code</title>
	</head>
	
	<body class="invoercode">

		<div class="sideNav">
			<ul>
				<a href="#">
					<div class="img"><img id="logo" src="Images/logo.png" alt="IJBurg College">
					</div>
				</a>
				<li><a href='controlpanel.php'><span>Home</span></a></li>
				<li><a href='invoercode.php'><span>Quizzes</span></a></li>
				<li><a href='#'><span>Statistieken</span></a></li>
				<li><a href='#'><span>Instellingen</span></a></li>
				<li><a href='#'><span>Log uit</span></a></li>
			</ul>
		</div>
		<div class="topNav">
		</div>
		
		<div class="border">
			<form method="" action="invoercodeScript.php">
				<div class="text1">
					<font size="10"><b>Voer de quiz pin in</b></font>
				</div>
				<input type="text" class="tekstvak" name="invoercode" placeholder="Pin"/>
				<input type="submit" class="button" name="button1"/><font size="10">Enter</font>
			</form>
		</div>

	</body>
</html>
