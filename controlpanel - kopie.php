<!-- Author: Team IJB01 Jesse Klijn
Laatste edit: 11-05-16 -->
<?php 
session_start();
if ($_SESSION['login-check'] != "1"){
	
	header('Location: index.php');
}
?>
<?php
// de limit variable is de hoeveelheid secondes van inactiviteit voordat je sessie gestopt wordt.
$limit = 10;
//Als de sessie variable $_SESSION['authenticated'] null is, wordt je teruggestuurd naar index.php
if (!isset($_SESSION['authenticated'])){
	header ('Location: index.php');
	exit;
}
// deze elseif zal je terugsturen naar de index.php pagina nadat $limit aantal secondes geen activiteit is geweest, expired is gezet op true
elseif ($_SESSION['authenticated'] + $limit < time()){
	$_SESSION = [];
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time() - 86400, '/');
}
session_destroy();
header('Location: index.php?expired=true');
exit;
	
}
//als er wel activiteit is zal $_SESSION['authenticated'] gereset worden.
else {
	$_SESSION['authenticated'] = time();
	}
?>

	
<?php

$username = "Hans";
$type = "Leraar";


?>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="mainStyleSheet.css" />
		<link rel="stylesheet" type="text/css" href="controlPanelStyleSheet.css" />
		<link rel="stylesheet" type="text/css" href="invoercodeStyleSheet.css" />
		<link rel="stylesheet" type="text/css" href="vragenConceptCSS.css" />
		<script type="text/javascript" src="controlPanel.js"></script>
		<script src="jquery1.12.3.js"></script>
		<script src="controlPanel.js"></script>
		<title> IJBattle Control Panel </title>
		
	</head>
	<font face="Helvetica">
	<body class="controlPanel">
	 
		<div class="sideNav">
			<ul>
				<a href="#">
					<div class="img"><img id="logo" src="Images/logo.png" alt="IJBurg College">
					</div>
				</a>
				<?php 
				
				echo "<li id='home'><a href='#'><span> Home</span></a></li>
				<li id='quizz'><a href='#'><span>Quiz spelen</span></a></li>";
				
				//TODO: SESSION vraag Jesse voor uitleg
				if($type == "Leerling"){
				echo"
				<li id='stats'><a href='#'><span>Statistieken</span></a></li>";
				}
				//TODO: SESSION
				if($type == "Leraar"){
				echo "
				<li id='quizBeheer'><a href='#'><span>Quiz beheer</span></a></li>
				<li id='leerlingBeheer'><a href='#'><span>Leerling beheer</span></a></li>
				";
				}
				
				echo "<li id='settings'><a href='#'><span>Instellingen</span></a></li>";
				
				echo "<li><a href='uitloggen.php'><span>Log uit</span></a></li>";
				
			
				?>
			</ul>
		</div>
		<div class="topNav">
		<div class="topContent">
			<font face="Helvetica" size="4">Welkom terug, <?php echo $username . "/ ". $type; ?> </font>
		</div>
		</div>
		<div id="content">
			<div class="panel">
			<!-- Hierin staat de html voor de pin-container -->
				<div class="pin-container">
					<form method="post" action="">
								<div class="title">
									<font size="10"><b>Voer de quiz pin in!</b></font>
								</div>
						<input type="text" id="textholder" name="pin" placeholder="Voer de quiz pin in bijv. 4321"/>
						</br>
						<input type="submit" id="startQuizz" name="button1" value="Start Quiz"/>
						<?php 
						if($message = null){ $message = "Voer je code in!"; }
						echo $message;
						?>
					</form>
				</div>
			</div>
			<div class="panel">
				<div id="quizz-question-container">
					<!--php code om de kleuren willekeurig te veranderen-->
					<?php 
						
						$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
						$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
						$color1 = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
						$color2 = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
						$color3 = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
						$color4 = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
					?>
					<!-- Div voor de doos om de vraag heen -->
					<div class= "Question">

					<h1> Wat is 't juiste antwoord? Klik op A </h1>

					</div>
					<!-- De knoppen op de pagina -->
					 <div class= "Vraag1" style="background: <?php echo $color1; ?>;"> <p> Het antwoord A </p> </div>
					 <div class= "Vraag2" style="background: <?php echo $color2; ?>;"> <p> Het antwoord B </p> </div>
					 <div class= "Vraag3" style="background: <?php echo $color3; ?>;"> <p> Het antwoord C </p> </div>
					 <div class= "Vraag4" style="background: <?php echo $color4; ?>;"> <p> Het antwoord D </p> </div>
					 
					 <div class= "Audio">
						<audio autoplay>
							<source src="Flilte.wav">
						</audio>
					</div>

				</div>
				
			</div>
			
			<div class="panel">
				<div id="stats-container">
				Stats
				</div>
				
			</div>
			<div class="panel">
				<div id="settings-container">
				Settings test
				</div>
				
			</div>
			<div class="panel">
				<div id="quizBeheer-container">
				Quiz Beheer
				</div>
				
			</div>
			<div class="panel">
				<div id="leerlingBeheer-container">
				Leerling Beheer
				</div>
				
			</div>
		</div>
		<div id="footer">
					<div id="footerFacebook"><iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FIJburg-College-deelschool-T2-981545421890715%2F%3Ffref%3Dts&width=450&layout=standard&action=recommend&show_faces=true&share=true&height=80&appId=813995018633406" 
						width="450" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
					</div>
					<div id="footerContent">
						<i><font size="2"> Hogeschool van Amsterdam - Team IJB001 - 2015/2016</font></i>
					</div>
					
					
		</div>
		
		</font>
	</body>
</html>
<?php
	echo '<script> hideAllContainers(); </script>';	
	
?>
<?php
	if($_SERVER['REQUEST_METHOD'] == 'POST') 
	{ 
		$Quizpin = 1111; 
		// Controle of benodigde velden wel ingevuld zijn 
		if(isset($_POST['pin'])) 
		{ 
		
			$IngevuldQuizPin = trim($_POST['pin']); 
			
			// Gebruikersnaam en wachtwoord controleren 
			if((int)$IngevuldQuizPin === $Quizpin){
				echo '<script> showQuiz(); </script>';
				die();
			}
			else if((int)$IngevuldQuizPin != (int)$Quizpin && $IngevuldQuizPin != 0){
				echo "<script> showPin(); </script>";
				echo "<script> alert('De code is onjuist! Probeer het opnieuw!');</script>";
				
				die();
			}
		}
	}
?>
