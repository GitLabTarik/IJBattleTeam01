<!-- Author: Team IJB01 Jesse Klijn
Laatste edit: 24-03-16 -->


<html> 
	<head>
		<!-- 
		Hier staan wat referenties naar andere documenten.
		-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="mainStyleSheet.css" />
		<link rel="stylesheet" type="text/css" href="homeStyleSheet.css" />
		<!-- Hier staat de titel die je bovenaan de html pagina ziet. -->
		<title> IJBattle Home </title>
	</head>
	<!-- Dit is de body. Hierin staat alle inhoud van de website die weergegeven wordt. --> 
	<body class="login">
	
		<div class='nav'>
			<ul>
				<a href="#">
				<div class="ijburglogo">
					<img id="logo" src="Images/logo.png" alt="IJBurg College">
				</div>
				</a>
				
				
			</ul>
		</div>
		<div id='content'>
		<!-- Hier zie je de login form met een list met invulboxes -->
			<div id='loginForm'>
				<h2>Welkom terug!</h3>
				<table width="300" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="" id="form">
					<tr>
						<form name="form1" method="post" action="">
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
									<tr>
										
										<!-- Errorbericht wanneer je sessie na inactiviteit terugstuurt naar de homepage -->
										<?php
										// deze zin zal alleen ge-echo'd worden als expired op true gezet wordt, dit gebeurt na de sessie gestopt wordt vanwege inactiviteit.
											if (isset($_GET['expired'])) {
												echo '<p style="color:#FF0000"><b>Uw sessie is verlopen, log alstublieft opnieuw in.</b></p>';
											}
										?>
									
									
										<td colspan="3"><strong>Vul hier je gegevens in </strong></td>
									</tr>
									<tr>
										<td width="78">Username</td>
										<td>:</td>
										<td width="294"><input name="myusername" type="text" id="myusername"></td>
									</tr>
									<tr>
										<td>Password</td>
										<td>:</td>
										<td><input name="mypassword" type="password" id="mypassword"></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td><input type="submit" name="Submit" value="Login" class="Submit"></td>
									</tr>
								</table>
							</td>
						</form>
					</tr>
				</table>
			</div>	
		</div>
		<?php
		//Hier komt nog php
		?>

	</body>

	</html>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	

session_start();


// Connect to DB
include('connect-db.php');




//Registreerd de username en wachtwoord en zet de username om naar kleine letters
if($_POST['myusername'] != null && $_POST['mypassword'] != null){
$inlognaam = strtolower($_POST['myusername']);
$wachtwoord = $_POST['mypassword'];



//een query aanroepen
$query =  sprintf("SELECT CONCAT(voornaam,achternaam), wachtwoord FROM leerling WHERE CONCAT(voornaam,achternaam) = '$inlognaam' AND wachtwoord = '$wachtwoord' ");

 $rows = mysqli_query($conn , $query);

    $numrows = mysqli_num_rows($rows); 

    if($numrows)
    {
        while($row = mysqli_fetch_assoc($rows))
        {
            $dbusername = $row['CONCAT(voornaam,achternaam)'];
            $dbpassword = $row['wachtwoord'];
            
        }
        
   
		$_SESSION['dbnaam'] = $inlognaam;
		$_SESSION['dbtype'] = "Leerling";
        $_SESSION['login-check']= "1";
		$_SESSION['authenticated']= time();
		header( "refresh:0;url=controlpanel.php" );
    }
	
	
    else {

	
	//een query aanroepen
$query =  sprintf("SELECT CONCAT(voornaam,achternaam), wachtwoord, idRol FROM docent WHERE CONCAT(voornaam,achternaam) = '$inlognaam' AND wachtwoord = '$wachtwoord' ");

 $rows = mysqli_query($conn , $query);

    $numrows = mysqli_num_rows($rows); 

    if($numrows)
    {
        while($row = mysqli_fetch_assoc($rows))
        {
            $dbusername = $row['CONCAT(voornaam,achternaam)'];
            $dbpassword = $row['wachtwoord'];
			$idRol = $row['idRol'];
            
        }
        
		if($idRol == 2){
			$_SESSION['dbtype'] = "Docent";
		}
		else if($idRol == 3){
			$_SESSION['dbtype'] = "Admin";
		}
		$_SESSION['dbnaam'] = $inlognaam;
        $_SESSION['login-check']= "1";
		$_SESSION['authenticated']= time();
		header( "refresh:0;url=controlpanel.php" );
    }
	
	

	

    else
	
		{	
		
			echo "<script type=\"text/javascript\">
            alert('Uw gebruikersnaam en/of wachtwoord zijn incorrect.');
            </script>";
			header( "refresh:0;url=index.php" );
		
	}
	
	
	}

}
	
	else{
		
		echo "<script type=\"text/javascript\">
            alert('Uw gebruikersnaam en/of wachtwoord zijn leeggelaten.');
            </script>";
		header( "refresh:0;url=index.php" );
		
	}
}
?>


	
