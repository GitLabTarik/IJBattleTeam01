<!--
Auteur: Cihad Palta, IB101, 5007291417
Project Agile Development team IJB101
Dit bestand is geschreven om docenten te weergeven in een tabel. De gegevens worden geselecteerd uit de database.
-->
<html>
   <head>
      <title>Docenten overzicht</title>
      <style>
         html * {
         font-family: Arial !important;
         }
         table {
         border-collapse: collapse;
         width: 100%;
         }
         th,
         td {
         text-align: left;
         padding: 8px;
         }
         tr:nth-child(even) {
         background-color: #f2f2f2
         }
         th {
         background-color: #483D8B;
         color: white;
         }
      </style>
   </head>
   <body>
<?php
         include('connect-db.php'); // Connect to DB
         
         $sql    = "SELECT * FROM docent
                     INNER JOIN
                     stamgroep
                     ON stamgroep.idStamgroep = docent.idStamgroep"; // Query to get all data from table
         $result = mysqli_query($conn, $sql); // Define variable result with the results from the query
         
         echo "<table border='1' cellpadding='10'>"; // Create the table
         echo "<tr> <th>ID</th> <th>Voornaam</th> <th>Achternaam</th> <th>Stamgroep</th>
             <th>Deelschool</th> <th>E-mail</th></tr>"; // Table headers
         
         if (mysqli_num_rows($result) > 0) { // Whileloop through results of database query, displaying them in the table
             
             while ($row = mysqli_fetch_assoc($result)) { // Whileloop through results of database query, displaying them in the table
                 
                 echo "<tr>"; // Print the data in the table rows
                 echo '<td>' . $row['idDocent'] . '</td>';
                 echo '<td>' . $row['voornaam'] . '</td>';
                 echo '<td>' . $row['achternaam'] . '</td>';
                 echo '<td>' . $row['naamStamgroep'] . '</td>';
                 echo '<td>' . $row['deelschool'] . '</td>';
                 echo '<td>' . $row['email'] . '</td>';
                 echo '<td><a href="docentBewerken.php?id=' . $row['idDocent'] . '">Bewerken</a></td>';
                 echo '<td><a href="docentVerwijderen.php?id=' . $row['idDocent'] . '">Verwijderen</a></td>';
                 echo "</tr>";
             }
             
         } else { // If there are no results echo no results
             
             echo "Er zijn geen docenten gevonden";
             
         }
         
         echo "</table>"; // Close table
         
?>
      <p><a href="docentNieuw.php">Nieuwe docent toevoegen</a></p>
   </body>
</html>