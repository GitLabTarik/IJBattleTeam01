<!--
Auteur: Cihad Palta, IB101, 5007291417
Project Agile Development team IJB101
Dit bestand is geschreven om leerlingen te verwijderen uit de database.
-->

<?php
session_start();
// connect to the database
include('connect-db.php');

// check if the 'id' variable is set in URL, and check that it is valid
if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    // get id value
    $id = $_GET['id'];
    
    // delete the entry
    
    $result = mysqli_query($conn, "DELETE FROM leerling WHERE idLeerling=$id"); //or die(mysql_error());
    
    // redirect back to the view page
$tekst = "leerlingBeheer-container";
$_SESSION['container'] = $tekst;

 header("Location: controlpanel.php"); 
} else
// if id isn't set, or isn't valid, redirect back to view page
    {
	$tekst = "leerlingBeheer-container";
	$_SESSION['container'] = $tekst;

	 header("Location: controlpanel.php"); 
}

?>