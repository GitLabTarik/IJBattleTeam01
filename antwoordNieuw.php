 <?php
   function renderFormAntwoord($idVraag, $antwoord, $goedAntwoord, $error) {
   ?>
<html>
   <title>Nieuwe antwoord invoeren</title>
   <body>
   
   	<link rel="stylesheet" type="text/css" href="formStyle.css" />
   
      <?php
         if ($error != '') // IF there are any errors, display them
             {
             echo '<div style="padding:4px; border:1px solid red; color:red;">' . $error . '</div>';
         }
         
         ?>
   </body>
   <form action="" method="post" class="form-style">
      <div>
         <label>idVraag: *</label>
         
		 <?php	
		include('connect-db.php');		 
		$sql = "SELECT * FROM vraag";
		$result = mysqli_query($conn, $sql);

		echo '<select name="idVraag">';
		while ($row = mysqli_fetch_array($result)) {
			echo "<option value='" . $row['vraag'] . "'>" . $row['vraag'] . "</option>";
		}
		echo "</select>";
        ?>
		 
		 <br/>
		 <strong>Antwoord 1: *</strong><br/>
         <input type="text" name="antwoord1"> <input type="checkbox" name="goedAntwoord1" value="0">
		<br/>
		<strong>Antwoord 2: *</strong><br/>
         <input type="text" name="antwoord2"> <input type="checkbox" name="goedAntwoord2" value="0">
		<br/>
		<strong>Antwoord 3: *</strong><br/>
         <input type="text" name="antwoord3"> <input type="checkbox" name="goedAntwoord3" value="0">
		<br/>
		<strong>Antwoord 4: *</strong><br/>
         <input type="text" name="antwoord4"> <input type="checkbox" name="goedAntwoord4" value="0">
		<br/>

    
         

         <p>* required</p>
         <input type="submit" name="submit" value="Opslaan">
      </div>
   </form>
</html>
<?php
   }
   include('connect-db.php'); // Connect to the database
   
   if (isset($_POST['submit'])) // Checks if the submit button has been pressed
       { // Gets the data filled in the form and making sure its valid
       $idVraag	       = mysqli_real_escape_string($conn, $_POST['idVraag']);
       $antwoord1      = mysqli_real_escape_string($conn, $_POST['antwoord1']);
	   $antwoord2      = mysqli_real_escape_string($conn, $_POST['antwoord2']);
	   $antwoord3      = mysqli_real_escape_string($conn, $_POST['antwoord3']);
	   $antwoord4      = mysqli_real_escape_string($conn, $_POST['antwoord4']);
       $goedAntwoord1     = isset($conn, $_POST['goedAntwoord1']);
	   $goedAntwoord2     = isset($conn, $_POST['goedAntwoord2']);
	   $goedAntwoord3     = isset($conn, $_POST['goedAntwoord3']);
	   $goedAntwoord4     = isset($conn, $_POST['goedAntwoord4']);
   
       
       // IF statement to check everything is filled in
       if ($idVraag == '' || $antwoord1 == '' || $antwoord2 == '') {
           
           $error = 'Vul a.u.b. alle gegevens in!'; // Error message if some field(s) are not filled in
           
           renderFormAntwoord($idVraag, $antwoord1, $antwoord2, $antwoord3, $antwoord3, $antwoord4, $goedAntwoord1, $goedAntwoord2, $goedAntwoord3, $goedAntwoord4, $error); // Show form again to continue filling in
       } else // IF everything is filled in
           {
			   
			   $sql = "select v.idVraag FROM vraag v where v.vraag = '$idVraag'";
				$result = mysqli_query($conn, $sql);
				  $row=$result->fetch_assoc();
		$id = intval($row['idVraag']);
           // Insert query with the filled in variables
            mysqli_query($conn, "INSERT antwoord SET idVraag=$id, antwoord='$antwoord1', goedAntwoord='$goedAntwoord1'");
			mysqli_query($conn, "INSERT antwoord SET idVraag=$id, antwoord='$antwoord2', goedAntwoord='$goedAntwoord2'");
			mysqli_query($conn, "INSERT antwoord SET idVraag=$id, antwoord='$antwoord3', goedAntwoord='$goedAntwoord3'");
			mysqli_query($conn, "INSERT antwoord SET idVraag=$id, antwoord='$antwoord4', goedAntwoord='$goedAntwoord4'");		   //or die(mysql_error());
           
           header("Location: vraagWeergeven.php"); // Once saved, redirect back to vraagWeergeven.php
       }
   } else // IF nothing is submitted just display an empty form
       {
       renderFormAntwoord('', '', '', '');
   }
   ?>